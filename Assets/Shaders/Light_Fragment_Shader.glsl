#version 460

out vec4 FragColor;

uniform vec3 color;
uniform float intensity;

void main()
{
	vec3 ambiant = intensity * color;

	FragColor = vec4(color, 1.0);
} 