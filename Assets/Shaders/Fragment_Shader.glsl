#version 460
struct Material {
	sampler2D ambiant; // object color under ambiant light
	sampler2D diffuse; // object color under diffuse light
	sampler2D specular; // color specular have impact on
	sampler2D emission; // emission map (emit light)
	float shininess; // specular pow
};

struct DirectionalLight {
	vec3 direction; // for directionalLight

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};


struct Light {
	vec3 position;
	vec3 direction; // for directionalLight

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	// For Point Light
	float constant;
	float linear;
	float quadratic;

	// For Spot Light
	float innerCutoff;
	float outerCutoff;

	int type;
};

out vec4 FragColor;

#define NR_LIGHTS 4

uniform sampler2D texture_diffuse1;
uniform sampler2D texture_diffuse2;
uniform sampler2D texture_diffuse3;
uniform sampler2D texture_specular1;
uniform sampler2D texture_specular2;
uniform sampler2D texture_specular3;
uniform vec3 globalColor;
uniform vec3 viewPosition;
uniform Material material;
uniform Light lights[NR_LIGHTS];
uniform DirectionalLight DLight;

in vec2 texCoord;
in vec3 normals;
in vec3 position; // the fragment position in space

vec3 CalcDirLight(DirectionalLight light, vec3 in_normals, vec3 viewDirection);
vec3 CalcOtherLight(Light light, vec3 in_normals, vec3 in_position, vec3 viewDirection);

void main()
{
	/*vec3 norm = normalize(normals);
	vec3 viewDir = normalize(viewPosition - position);

	vec3 result = CalcDirLight(DLight, norm, viewDir);

	for(int i = 0; i < NR_LIGHTS; i++)
		result += CalcOtherLight(lights[i], norm, position, viewDir);

	FragColor = vec4(result, 1.0);*/
	FragColor = texture(texture_diffuse1, texCoord);
	//FragColor = vec4(1.0, 1.0, 1.0, 1.0);
}

vec3 CalcDirLight(DirectionalLight light, vec3 in_normals, vec3 viewDirection) {

	vec3 LightDir = normalize(-light.direction);

	float diff = max(dot(in_normals, LightDir), 0.0);

	vec3 reflectDir = reflect(-LightDir, in_normals);
	float spec = pow(max(dot(viewDirection, reflectDir), 0.0), material.shininess);

	vec3 ambiant = light.ambient * vec3(texture(texture_diffuse1, texCoord));
	vec3 diffuse = light.diffuse * diff * vec3(texture(texture_diffuse1, texCoord));
	vec3 specular = light.specular * spec * vec3(texture(texture_specular1, texCoord));

	return (ambiant + diffuse + specular);

}

vec3 CalcOtherLight(Light light, vec3 in_normals, vec3 in_position, vec3 viewDirection) {
	
	vec3 ambient;
    vec3 diffuse;
    vec3 specular;
	
	if(light.type == 2) {
	
		vec3 lightDir = normalize(light.position - in_position);
		// diffuse shading
		float diff = max(dot(in_normals, lightDir), 0.0);
		// specular shading
		vec3 reflectDir = reflect(-lightDir, in_normals);
		float spec = pow(max(dot(viewDirection, reflectDir), 0.0), material.shininess);
		// attenuation
		float dist = length(light.position - in_position);
		float attenuation = 1.0 / (light.constant + light.linear * dist + light.quadratic * (dist * dist));    
		// combine results
		ambient = light.ambient * vec3(texture(texture_diffuse1, texCoord));
		diffuse = light.diffuse * diff * vec3(texture(texture_diffuse1, texCoord));
		specular = light.specular * spec * vec3(texture(texture_specular1, texCoord));

		ambient *= attenuation;
		diffuse *= attenuation;
		specular *= attenuation;    

	} else {
	
		 vec3 lightDir = normalize(light.position - in_position);
		// diffuse shading
		float diff = max(dot(in_normals, lightDir), 0.0);
		// specular shading
		vec3 reflectDir = reflect(-lightDir, in_normals);
		float spec = pow(max(dot(viewDirection, reflectDir), 0.0), material.shininess);
		// attenuation
		float dist = length(light.position - in_position);
		float attenuation = 1.0 / (light.constant + light.linear * dist + light.quadratic * (dist * dist));    
		// spotlight intensity
		float theta = dot(lightDir, normalize(-light.direction)); 
		float epsilon = light.innerCutoff - light.outerCutoff;
		float intensity = clamp((theta - light.outerCutoff) / epsilon, 0.0, 1.0);
		// combine results
		ambient = light.ambient * vec3(texture(texture_diffuse1, texCoord));
		diffuse = light.diffuse * diff * vec3(texture(texture_diffuse1, texCoord));
		specular = light.specular * spec * vec3(texture(texture_specular1, texCoord));

		ambient *= attenuation * intensity;
		diffuse *= attenuation * intensity;
		specular *= attenuation * intensity;

	}

	return (ambient + diffuse + specular);

}