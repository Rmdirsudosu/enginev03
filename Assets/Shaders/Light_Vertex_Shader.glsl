#version 460

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec4 inColor;
layout (location = 2) in vec2 uvs;

uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 projection_matrix;

out vec4 color;
out vec2 texCoord;

void main()
{
    gl_Position = projection_matrix * view_matrix * model_matrix * vec4(inPos, 1.0);

	color = inColor;

	texCoord = uvs;
}