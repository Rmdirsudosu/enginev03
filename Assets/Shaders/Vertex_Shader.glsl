#version 460

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inNormals;
layout (location = 2) in vec2 uvs;

uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 projection_matrix;

out vec4 color;
out vec2 texCoord;
out vec3 normals;
out vec3 position;

void main()
{
    gl_Position = projection_matrix * view_matrix * model_matrix * vec4(inPos, 1.0);

	// Revert the model_matrix do only keep scaling and rotation
	// and be sure that non uniform scaling won't affect the normals.
	// This adapt the normals to world space
	normals = mat3(transpose(inverse(model_matrix))) * inNormals;
	position = vec3(model_matrix * vec4(inPos, 1.0));

	texCoord = uvs;
}

/* EXAMPLE OF VARIABLES USAGE

	SWIZZING
	vec2 someVec;
	vec4 differentVec = someVec.xyxx;
	vec3 anotherVec = differentVec.zyw;
	vec4 otherVec = someVec.xxxx + anotherVec.yxzy;

	vec2 vect = vec2(0.5, 0.7);
	vec4 result = vec4(vect, 0.0, 0.0);
	vec4 otherResult = vec4(result.xyz, 1.0);

	var type : 
		vecn: the default vector of n floats.
		bvecn: a vector of n booleans.
		ivecn: a vector of n integers.
		uvecn: a vector of n unsigned integers.
		dvecn: a vector of n double components.

*/