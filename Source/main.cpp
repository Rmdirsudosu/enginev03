#include "Core/EngineStarter.h"

using Engine::EngineStarter;

int main(int agrc, char ** argv) {

	EngineStarter starter = EngineStarter();
	starter.Init();
	starter.Run();

	return 0;

}