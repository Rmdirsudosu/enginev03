#include "EngineStarter.h"

using namespace Engine::Init;

Engine::EngineStarter::EngineStarter()
{
}

Engine::EngineStarter::~EngineStarter()
{
}

bool Engine::EngineStarter::Init()
{
	WindowInfo window = WindowInfo();
	ContextInfo context = ContextInfo();
	FramebufferInfo framebufferInfo = FramebufferInfo();
	Init_GLFW::init(window, context, framebufferInfo);

	// Set Managers
	m_scene_manager = new Scene_Manager();
	m_file_manager = new File_Manager();
	m_shader_manager = new Shader_Manager();
	m_material_manager = new Material_Manager();
	m_control_manager = new Control_Manager();

	if (m_scene_manager && m_shader_manager)
	{
		Init_GLFW::setListener(m_scene_manager);	
		Init_GLFW::setControl(m_control_manager);
		m_scene_manager->setShaderManager(m_shader_manager);
		m_scene_manager->setFileManager(m_file_manager);
		m_scene_manager->setSceneFile("azea"); // TODO
		m_models_manager = new Managers::Model_Manager();
		m_models_manager->setMaterialManager(m_material_manager);
		m_scene_manager->setModelsManager(m_models_manager);
		m_scene_manager->setControlManager(m_control_manager);
	}
	else
	{
		return false;
	}

	return true;
}

void Engine::EngineStarter::Run()
{
	Init_GLFW::run();
}

Scene_Manager * Engine::EngineStarter::GetScene_Manager() const
{
	return nullptr;
}

Shader_Manager * Engine::EngineStarter::GetShader_Manager() const
{
	return nullptr;
}

Model_Manager * Engine::EngineStarter::GetModels_Manager() const
{
	return nullptr;
}

void Engine::EngineStarter::setSceneFile(String filePath)
{
}
