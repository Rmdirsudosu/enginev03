#pragma once

#include "FileType.h"
#include <fstream>
#include "../Ressources/String.h"
#include "ConsoleInOut.h"

namespace Utils {

	class FileImporter {

	public:
		static String getFile(FileType type, const String& fileName);

	private:
		static String getStringForType(FileType type);

		static String ReadInFile(String fileName);

	};

}