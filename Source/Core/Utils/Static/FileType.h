#pragma once

enum FileType {

	G_TEXTS = 0,
	G_SCENES = 1,
	G_OBJECTS = 2,
	G_MATERIALS = 3,
	G_GLSL = 4,
	G_OBJ = 5,
	G_JPG = 6,
	G_BMP = 7,
	G_DDS = 8

};