#include "TextureLoader.h"
#include "../Libs/stb_image.h"
#include "ConsoleInOut.h"
#include <glad/glad.h>
#include <GLFW/glfw3.h>

namespace Utils {

	GLuint TextureLoader::LoadTexture(const String & fileName)
	{
		int width, height, nbrColor;
		unsigned char* data;
		data = stbi_load(fileName.toChars(), &width, &height, &nbrColor, 0);

		if (data) {
			unsigned int texture;
			glGenTextures(1, &texture);
			glBindTexture(GL_TEXTURE_2D, texture);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); // if scalling the texture down (_MINimize)
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // if scalling the texture up (_MAGnify)
			float maxAnisotropy;

			glTexImage2D(GL_TEXTURE_2D, // Texture type 2D, 3D, 1D, ..
				0, // minmap level
				GL_RGB, // how to store the values here we store them in RGB
				width, // Width of the image
				height, // height of the image
				0, // legacy should alway be 0 doesn't serve for anything
				GL_RGB, // source image color format
				GL_UNSIGNED_BYTE, // source image type (char are bytes)
				data); // the actual image data;
			glGenerateMipmap(GL_TEXTURE_2D);
		} else 
			ConsoleInOut::Out("Fail to load texture: " + fileName);		

		stbi_image_free(data);

		return 0;
	}

	TextureLoader::~TextureLoader()
	{
	}

}