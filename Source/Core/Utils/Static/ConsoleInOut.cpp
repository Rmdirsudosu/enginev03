#include "ConsoleInOut.h"

namespace Utils {

	/*void ConsoleInOut::Out(std::string S)
	{
		std::cout << S << std::endl;
	}*/

	void ConsoleInOut::Out(int s)
	{
		std::cout << s << std::endl;
	}

	void ConsoleInOut::Out(double s)
	{
		std::cout << s << std::endl;
	}

	void ConsoleInOut::Out(String s)
	{
		std::cout << s << std::endl;
	}

	void ConsoleInOut::Out(bool b)
	{
		std::cout << String::toString(b) << std::endl;
	}

	void ConsoleInOut::Out(const char * c)
	{
		std::cout << c << std::endl;
	}

	void ConsoleInOut::Out(const unsigned char * c)
	{
		std::cout << c << std::endl;
	}

	void ConsoleInOut::Out(String s, const unsigned char * c)
	{
		std::cout << s << c << std::endl;
	}

	void ConsoleInOut::Out(String s, GLenum en)
	{
		std::cout << s << en << std::endl;
	}

	void ConsoleInOut::Out(String s, const GLchar * gchar)
	{
		std::cout << s << gchar << std::endl;
	}

	void ConsoleInOut::Out(GLchar * gchar)
	{
		std::cout << gchar << std::endl;
	}

	void ConsoleInOut::Out(std::string s)
	{
		std::cout << s << std::endl;
	}

	//void ConsoleInOut::Out(glm::mat4 mat)
	//{
	//	for (int i = 0; i < mat.length(); i++) {
	//		std::cout << "| ";
	//		for (int j = 0; j < mat.length(); j++)
	//			std::cout << "\t" << mat[i][j] << " ";
	//		std::cout << "\t|" << std::endl;
	//	}
	//	std::cout << std::endl;
	//}

	std::string ConsoleInOut::In()
	{
		return std::string();
	}

}


