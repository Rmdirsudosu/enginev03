#include "FileImporter.h"

namespace Utils {

	String FileImporter::getFile(FileType type, const String & fileName)
	{
		String fileType = getStringForType(type);
		String name = fileName; // Don't know why but it suddently stop acception const String for contains
		if (name.contains(fileType))
			return ReadInFile(fileName);
		else {
			name += fileType;
			return ReadInFile(name);
		}

		return '\0';
	}

	String FileImporter::getStringForType(FileType type)
	{
		switch (type) {

			case G_TEXTS: return ".txt";
			case G_SCENES: return ".gscene";
			case G_OBJECTS: return ".gobject";
			case G_MATERIALS: return ".gmaterial";
			case G_GLSL: return ".glsl";
			case G_OBJ: return ".obj";
			default: return "WRONGTYPE";

		}
	}

	String FileImporter::ReadInFile(String fileName)
	{
		ConsoleInOut::Out(fileName);

		std::string Output;
		std::ifstream file(fileName.toChars(), std::ios::in);

		if (!file.good()) {
			ConsoleInOut::Out("Can't read file: " + fileName);
			terminate();
		}

		file.seekg(0, std::ios::end);
		Output.resize((unsigned int)file.tellg());
		file.seekg(0, std::ios::beg);
		file.read(&Output[0], Output.size());
		file.close();

		return Output.c_str();
	}

}
