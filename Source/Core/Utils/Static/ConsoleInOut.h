#pragma once

#include <string>
#include "../Ressources/String.h"
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
//#include "glm/glm.hpp"

namespace Utils {

	class ConsoleInOut {

	public:

		//static void Out(std::string S);
		static void Out(int s);
		static void Out(double s);
		static void Out(String s);
		static void Out(bool b);
		static void Out(const char* c);
		static void Out(const unsigned char* c);
		static void Out(String s, const unsigned char* c);
		static void Out(String s, GLenum en);
		static void Out(String s, const GLchar * gchar);
		static void Out(GLenum en);
		static void Out(GLchar * gchar);
		static void Out(std::string s);
		//static void Out(glm::mat4 mat);

		static std::string In();

	};
}

