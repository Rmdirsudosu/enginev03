#pragma once

#include "../Static/FileImporter.h"
#include "../Ressources/String.h"

namespace Utils {
	
	class TextureLoader {

	public:

		static GLuint LoadTexture(const String& fileName);

		~TextureLoader();

	};

}