#include "Init_GLFW.h"

using Utils::String;
using Utils::ConsoleInOut;
using Engine::Core::DebugOutput;

namespace Engine {

	namespace Init {
	
		WindowInfo Init_GLFW::window;
		GLFWwindow* Init_GLFW::activeWindow;
		IListener* Init_GLFW::listener = NULL;
		IControl* Init_GLFW::control = NULL;
		float Init_GLFW::deltaTime = 0;
		float Init_GLFW::last = 0;

		void Init_GLFW::init(const WindowInfo & windowInfo, const ContextInfo & contextInfo, const FramebufferInfo & framebufferInfo)
		{
			window = windowInfo;

			glfwInit();
			glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, contextInfo.major_version);
			glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, contextInfo.minor_version);
			glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
			
			if (window.bisFullscreen) {
				GLFWmonitor* primary = glfwGetPrimaryMonitor();
				const GLFWvidmode* mode = glfwGetVideoMode(primary);
				window.height = mode->height;
				window.width = mode->width;
				activeWindow = glfwCreateWindow(mode->width, mode->height, window.name,
					primary, // fullscreen
					NULL); // share ressources
				/*mode = glfwGetVideoMode(primary);
				glfwSetWindowMonitor(activeWindow, primary, 0, 0, mode->width, mode->height, mode->refreshRate);*/
			}
			else {
				activeWindow = glfwCreateWindow(window.width, window.height, window.name,
					NULL, // fullscreen
					NULL); // share ressources
			}
			
			if (activeWindow == NULL) {
				ConsoleInOut::Out("Failed to create a GLFW window.");
				glfwTerminate();
			}
			glfwMakeContextCurrent(activeWindow);
			
			if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
				ConsoleInOut::Out("Failed to initialize GLAD");
			}

			glfwSetInputMode(activeWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

			// SETTING CALLBACKS FUNCTIONS 
			glfwSetFramebufferSizeCallback(activeWindow, reshapeCallback);
			glfwSetWindowRefreshCallback(activeWindow, idleCallback);
			glfwSetCursorPosCallback(activeWindow, mouseInput);
			glfwSetScrollCallback(activeWindow, scrollInput);

			// Parameter Location left/right, location up/down here they're set so the window is in the top left corner
			// to put the window in the center you could enter 0.5 instead of 0 (screen / 2)
			glViewport(0, 0, window.width, window.height);

			//glDebugMessageCallback(DebugOutput::myCallBack, NULL);
			//glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, GL_TRUE);
			
		}

		void Init_GLFW::run()
		{
			control->notifyGameStart();
			while (!glfwWindowShouldClose(activeWindow)) {
				tick();
				processInput(activeWindow);

				displayCallback();				
				glfwPollEvents();
			}
			closeCallback();
		}

		void Init_GLFW::printOpenGLInfo(const WindowInfo & windowInfo, const ContextInfo & contextInfo)
		{
		}

		void Init_GLFW::tick()
		{
			float current = glfwGetTime();
			deltaTime = current - last;
			last = current;
		}

		void Init_GLFW::idleCallback(GLFWwindow* activeWin)
		{
		}

		void Init_GLFW::displayCallback(void)
		{
			if (listener) {
				tick();
				listener->notifyBeginFrame();
				listener->notifyDisplayFrame();

				glfwSwapBuffers(activeWindow);
				glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
				glClear(GL_COLOR_BUFFER_BIT);

				listener->notifyEndFrame();
			}
		}

		void Init_GLFW::reshapeCallback(GLFWwindow* activeWin, int width, int height)
		{
			if (window.bisReshapable) {
				if (listener) {
					listener->notifyReshape(width, height, window.width, window.height);
				}
				window.width = width;
				window.height = height;
			}
			control->notifyReshape(width, height);
			glViewport(0, 0, width, height);
		}

		void Init_GLFW::closeCallback()
		{
			glfwTerminate();
		}

		void Init_GLFW::processInput(GLFWwindow * activeWin)
		{
			control->notifyKeyPress(activeWin, deltaTime);
		}

		void Init_GLFW::mouseInput(GLFWwindow * activeWin, double xpos, double ypos)
		{
			control->notifyMouseInput(xpos, ypos, deltaTime);
		}

		void Init_GLFW::scrollInput(GLFWwindow * activeWin, double xoffset, double yoffset)
		{
			control->notifyScrollInput(xoffset, yoffset, deltaTime);
		}

		void Init_GLFW::setControl(Control_Manager *& inControl)
		{
			control = inControl;
		}

		void Init_GLFW::setListener(Scene_Manager *& iListener)
		{
			listener = iListener;
		}

	}

}
