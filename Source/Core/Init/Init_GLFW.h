#pragma once

#include "ContextInfo.h"
#include "FramebufferInfo.h"
#include "WindowInfo.h"
#include "FramebufferInfo.h"
#include "../Utils/Ressources/String.h"
#include "../Utils/Static/ConsoleInOut.h"
#include "IListener.h"
#include "../Managers/Scene_Manager.h"
#include "../Controls/IControl.h"
#include "../Managers/Control_Manager.h"
#include "DebugOutput.h"

using namespace Engine::Init;
using Engine::Core::IListener;
using Engine::Managers::Scene_Manager;
using Engine::Controls::IControl;
using Engine::Managers::Control_Manager;

namespace Engine {

	namespace Init {
		class Init_GLFW {

		public:
			static void init(const WindowInfo& windowInfo, const ContextInfo& contextInfo, const FramebufferInfo& framebufferInfo);

			static void run();

			static void printOpenGLInfo(const WindowInfo& windowInfo, const ContextInfo& contextInfo);

		private:
			// VARIABLES
			static WindowInfo window;
			static GLFWwindow* activeWindow;
			
			// Listener
			static IListener* listener;
			static IControl* control;

			static float deltaTime;
			static float last;

			// FUNCTIONS
			static void tick();

			// CALLBACKS
			static void idleCallback(GLFWwindow* activeWin);
			static void displayCallback(void);
			static void reshapeCallback(GLFWwindow * activeWin, int width, int height);
			static void closeCallback();

			// INPUT
			static void processInput(GLFWwindow* activeWin);
			static void mouseInput(GLFWwindow* activeWin, double xpos, double ypos);
			static void scrollInput(GLFWwindow* activeWin, double xoffset, double yoffset);

			// LISTENER FUNCTIONS
		public:
			static void setListener(Scene_Manager*& iListener);
			static void setControl(Control_Manager*& inControl);

		};
	}

}
