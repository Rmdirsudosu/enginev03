#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <assert.h>
#include "../Utils/Static/ConsoleInOut.h"

using namespace Utils;
namespace Engine {

	namespace Core {

		class DebugOutput {

		public:
			DebugOutput() {};
			~DebugOutput() {};

			static void CALLBACK myCallBack(GLenum source, GLenum type, GLenum id, GLenum severity, GLsizei length,
				const GLchar *msg, const void *userParam) {

				ConsoleInOut::Out("\n**********Debug Output**************");
				ConsoleInOut::Out(getStringForSource(source));
				ConsoleInOut::Out(getStringFroType(type));
				ConsoleInOut::Out(getStringForSeverity(severity));
				ConsoleInOut::Out("Debug call: ", msg);
				ConsoleInOut::Out("\n************************************");


			}

			static String getStringFroType(GLenum type) {

				String out1 = "Type: ";
				switch (type) {

				case GL_DEBUG_TYPE_ERROR:
					return out1 + "Error";
				case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
					return out1 + "Deprecated behavior";
				case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
					return out1 + "Undefined behavior";
				case GL_DEBUG_TYPE_PORTABILITY:
					return out1 + "Portability issue";
				case GL_DEBUG_TYPE_PERFORMANCE:
					return out1 + "Performance issue";
				case GL_DEBUG_TYPE_MARKER:
					return out1 + "Stream annotation";
				/*case GL_DEBUG_TYPE_OTHER_ARB:
					return out1 + "Other";*/
				default:
					//assert(false);
					return out1 + "";
				}
			}

			static String getStringForSource(GLenum source) {

				String out1 = "Source: ";
				switch (source) {
				case GL_DEBUG_SOURCE_API:
					return out1 + "API";
				case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
					return out1 + "Window system";
				case GL_DEBUG_SOURCE_SHADER_COMPILER:
					return out1 + "Shader compiler";
				case GL_DEBUG_SOURCE_THIRD_PARTY:
					return out1 + "Third party";
				case GL_DEBUG_SOURCE_APPLICATION:
					return out1 + "Application";
				case GL_DEBUG_SOURCE_OTHER:
					return out1 + "Other";
				default:
					assert(false);
					return out1 + "";
				}
			}

			static String getStringForSeverity(GLenum severity) {

				String out1 = "Severity: ";
				switch (severity) {
				case GL_DEBUG_SEVERITY_HIGH:
					return out1 + "High";
				case GL_DEBUG_SEVERITY_MEDIUM:
					return out1 + "Medium";
				case GL_DEBUG_SEVERITY_LOW:
					return out1 + "Low";
				case GL_DEBUG_SEVERITY_NOTIFICATION:
					return out1 + "Notification";
				default:
					assert(false);
					return(out1 + "");
				}
			}

		};

	}

}
