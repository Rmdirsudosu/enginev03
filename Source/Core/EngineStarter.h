#pragma once

#include "Managers/Scene_Manager.h"
#include "Utils/Ressources/String.h"
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "Init/Init_GLFW.h"

using Utils::String;
using namespace Engine::Managers;

namespace Engine {

	class EngineStarter
	{
	public:
		EngineStarter();
		~EngineStarter();

		//OpenGL and manager init
		bool Init();

		//Loop
		void Run();

		//Getters
		Scene_Manager*  GetScene_Manager()  const;
		Shader_Manager* GetShader_Manager() const;
		Model_Manager* GetModels_Manager() const;

		void setSceneFile(String filePath);

	private:
		Scene_Manager*  m_scene_manager;
		Shader_Manager* m_shader_manager;
		Model_Manager* m_models_manager;
		File_Manager* m_file_manager;
		Material_Manager* m_material_manager;
		Control_Manager* m_control_manager;

	};

}