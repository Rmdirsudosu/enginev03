#include "FPSCamera.h"

namespace Engine {

	namespace Controls {

		namespace Player {

			FPSCamera::FPSCamera() {			
				width = 1280;
				height = 720;
				position = glm::vec3(0.0, 0.0, 3.0);
				rotation = glm::vec3(0.0, 0.0, 0.0);
				direction = glm::vec3(0.0, 0.0, 0.0);
				up = glm::vec3(0.0f, 1.0f, 0.0f);
				front = glm::vec3(0.0f, 0.0f, -1.0f);
				bOrthographic = false;
				FoV = 45.0f;
				minRenderingDistance = 0.1f;
				maxRenderingDistance = 100.0f;
				lastX = width / 2;
				lastY = height / 2;
				sensivity = 0.05f;
				yaw = -90.0f;
				pitch = 0.0f;
				gameStarted = false;
			}

			FPSCamera::~FPSCamera()
			{
			}

			void FPSCamera::update()
			{
				glm::mat4 view_matrix(1);
				view = glm::lookAt(position,
					position + front, // Direction 
					up); //
				glm::mat4 projection_matrix(1);
				projection = glm::perspective(glm::radians(FoV), // Field of view
					(float)width / (float)height, // screen width and height in float
					minRenderingDistance, // start of the frustum
					maxRenderingDistance); // end of the frustum (to make it simple render distance)
			}

			void FPSCamera::moveForward(float speed)
			{
				position += speed * front;
				update();
			}

			void FPSCamera::straf(float speed)
			{
				position += glm::normalize(glm::cross(front, up)) * speed;
				update();
			}

			void FPSCamera::moveUpward(float speed)
			{
				position += speed * up;
				update();
			}

			void FPSCamera::mouseInput(double xpos, double ypos, float deltaTime)
			{
				if (!gameStarted) {
					gameStarted = true;
					lastX = xpos;
					lastY = ypos;
				}

				float xoffset = xpos - lastX;
				float yoffset = lastY - ypos;
				lastX = xpos;
				lastY = ypos;

				xoffset *= sensivity;
				yoffset *= sensivity;

				yaw += xoffset;
				pitch += yoffset;
				if (pitch > 45.0f)
					pitch = 45.0f;
				if (pitch < -45.0f)
					pitch = -45.0f;

				glm::vec3 cfront;
				cfront.x = cos(glm::radians(pitch)) * cos(glm::radians(yaw));
				cfront.y = sin(glm::radians(pitch));
				cfront.z = cos(glm::radians(pitch)) * sin(glm::radians(yaw));

				front = glm::normalize(cfront);

				update();
			}

			void FPSCamera::scrollInput(double xoffset, double yoffset, float deltaTime)
			{
				if (FoV >= 1.0f && FoV <= 45.0f)
					FoV -= yoffset;
				if (FoV <= 1.0f)
					FoV = 1.0f;
				if (FoV >= 45.0f)
					FoV = 45.0f;

				update();
			}

			void FPSCamera::setSensivity(float sens)
			{
				sensivity = sens;
			}

			glm::vec3 FPSCamera::getPlayerPosition()
			{
				return position;
			}

			glm::vec3 FPSCamera::getPlayerRotation()
			{
				return rotation;
			}

			glm::vec3 FPSCamera::getPlayerDirection()
			{
				return front;
			}

			void FPSCamera::getPlayerPositionAndRotation(glm::vec3 & outPosition, glm::vec3 & outRotation)
			{
				outPosition = position;
				outRotation = rotation;
			}

		}

	}

}