#pragma once

#include "../Rendering/Cameras/Camera.h"
#include "../Rendering/Cameras/ICamera.h"

using Engine::Rendering::Cameras::Camera;
using Engine::Rendering::Cameras::ICamera;

namespace Engine {

	namespace Controls {
	
		namespace Player {
		
			class FPSCamera : public Camera, public ICamera {
			public:

				FPSCamera();
				~FPSCamera();

				virtual void update();

				void moveForward(float speed);
				void straf(float speed);
				void moveUpward(float speed);
				void mouseInput(double xpos, double ypos, float deltaTime);
				void scrollInput(double xoffset, double yoffset, float deltaTime);

				void setSensivity(float sens);

				void getPlayerPositionAndRotation(glm::vec3 & outPosition, glm::vec3 & outRotation);
				glm::vec3 getPlayerPosition();
				glm::vec3 getPlayerRotation();
				glm::vec3 getPlayerDirection();

			private:
				glm::vec3 front;

				double lastX;
				double lastY;

				float yaw;
				float pitch;

				float sensivity;

				bool gameStarted;

			};

		}

	}

}