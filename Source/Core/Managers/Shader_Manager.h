#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <fstream>
#include <ioStream>
#include <map>
#include <vector>
#include "../Utils/Static/FileImporter.h"
#include "../Utils/Ressources/String.h"
#include "../Rendering/Shader/IShader.h"

using Utils::String;

namespace Engine {

	namespace Managers {

		class Shader_Manager {

		private:
			String ReadShader(const String& filename);
			GLuint CreateShader(GLenum shaderType, String source, String shaderName);

		public:
			// Constructor
			Shader_Manager(void);
			// Destructor
			~Shader_Manager(void);

			void CreateProgram(const String& shaderName, const String& vertexShaderFilename, const String& fragmentShaderFilename);

			static const GLuint GetShader(const String& shaderName);

			void deleteShader(const String& shaderName);

		private:
			static std::map<String, GLuint> programs;

			bool checkIfNotExist(const String& shaderName);
			static bool staticCheckIfNotExist(const String& shaderName);

		};

	}

}
