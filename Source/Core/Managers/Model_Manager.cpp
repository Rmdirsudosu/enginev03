#include "Model_Manager.h"

using namespace Engine::Rendering;
using namespace Models;
using namespace Lights;

namespace Engine {

	namespace Managers {

		Model_Manager::Model_Manager()
		{

			glm::vec3 cubePositions[] = {
			  glm::vec3(0.0f, 0.0f, 0.0f),
			  glm::vec3(2.0f,  5.0f, -15.0f),
			  glm::vec3(-1.5f, -2.2f, -2.5f),
			  glm::vec3(-3.8f, -2.0f, -12.3f),
			  glm::vec3(2.4f, -0.4f, -3.5f),
			  glm::vec3(-1.7f,  3.0f, -7.5f),
			  glm::vec3(1.3f, -2.0f, -2.5f),
			  glm::vec3(1.5f,  2.0f, -2.5f),
			  glm::vec3(1.5f,  0.2f, -1.5f),
			  glm::vec3(-1.3f,  1.0f, -1.5f)
			};

			String cubeName = "cube";

			/*CubeIndex* cube = new CubeIndex();
			cube->SetProgram(Shader_Manager::GetShader("colorShader"));
			cube->SetTexture("container", Material_Manager::getTexture("container"));
			cube->SetTexture("awesomeface", Material_Manager::getTexture("awesomeface"));
			cube->Create();
			cube->SetPosition(cubePositions[0]);
			String name = cubeName + String::toString(15);
			gameModelList_NDC[name] = cube;*/

			light0 = new Light();
			light0->setDirection(glm::vec3(-0.2f, -1.0f, -0.3f));
			light0->setAmbiant(glm::vec3(0.05f));
			light0->setDiffuse(glm::vec3(0.4f));
			light0->setSpecular(glm::vec3(0.5f));
			light0->setLightIntensity(0.2f);
			light0->setType(G_DIRECTIONAL);

			light1 = new Light();
			light1->setPosition(glm::vec3(0.7f, 0.2f, 2.0f));
			light1->setDirection(glm::vec3(-0.2f, -1.0f, -0.3f));
			light1->setAmbiant(glm::vec3(0.05f));
			light1->setDiffuse(glm::vec3(0.8f));
			light1->setSpecular(glm::vec3(1.0f));
			light1->setLightIntensity(0.2f);
			light1->setFade(1.0f, 0.09f, 0.023f);
			light1->setCone(0.0f, 0.0f);
			light1->setType(G_POINT);

			light2 = new Light();
			light2->setPosition(glm::vec3(2.3f, -3.3f, -4.0f));
			light2->setDirection(glm::vec3(-0.2f, -1.0f, -0.3f));
			light2->setAmbiant(glm::vec3(0.05f));
			light2->setDiffuse(glm::vec3(0.8f));
			light2->setSpecular(glm::vec3(1.0f));
			light2->setLightIntensity(0.2f);
			light2->setFade(1.0f, 0.09f, 0.023f);
			light2->setCone(0.0f, 0.0f);
			light2->setType(G_POINT);

			light3 = new Light();
			light3->setPosition(glm::vec3(-4.0f, 2.0f, -12.0f));
			light3->setDirection(glm::vec3(-0.2f, -1.0f, -0.3f));
			light3->setAmbiant(glm::vec3(0.05f));
			light3->setDiffuse(glm::vec3(0.8f));
			light3->setSpecular(glm::vec3(1.0f));
			light3->setLightIntensity(0.2f);
			light3->setFade(1.0f, 0.09f, 0.023f);
			light3->setCone(0.0f, 0.0f);
			light3->setType(G_POINT);

			light4 = new Light();
			light4->setPosition(glm::vec3(0.0f, 0.0f, 3.0f));
			light4->setDirection(glm::vec3(0.0f, 0.0f, -1.0f));
			light4->setAmbiant(glm::vec3(0.0f));
			light4->setDiffuse(glm::vec3(1.0f));
			light4->setSpecular(glm::vec3(1.0f));
			light4->setLightIntensity(0.2f);
			light4->setFade(1.0f, 0.09f, 0.032f);
			light4->setCone(12.5f, 15.0f);
			light4->setType(G_SPOT);
			

			for (int i = 0; i < 10; i++) {
				CubeIndex* cube = new CubeIndex();
				cube->SetProgram(Shader_Manager::GetShader("colorShader"));
				cube->SetTexture("box", material_manager->getTexture("box"));
				cube->SetTexture("spec", material_manager->getTexture("spec"));
				cube->Create();
				cube->setLights(light0);
				cube->setLights(light1);
				cube->setLights(light2);
				cube->setLights(light3);
				cube->setLights(light4);
				cube->SetPosition(cubePositions[i]);
				cube->SetRotation(20.0f * i, cubePositions[i]+glm::vec3(0.01f, 0.01f, 0.01f));
				String name = cubeName + String::toString(i);
				gameModelList[name] = cube;
			}

			
		}

		Model_Manager::~Model_Manager()
		{
			for (auto model : gameModelList)
			{
				delete model.second;
			}
			gameModelList.clear();

			for (auto model : gameModelList_NDC)
			{
				delete model.second;
			}
			gameModelList_NDC.clear();
		}

		void Model_Manager::Draw()
		{
			if (!gameModelList_NDC.empty())
				for (auto model : gameModelList_NDC) {
					model.second->Draw();
				}
		}

		void Model_Manager::Draw(const glm::mat4 & projection_matrix, const glm::mat4 & view_matrix, glm::vec3& cameraPosition, glm::vec3& cameraDirection)
		{
			glm::vec3 ambiant, diffuse, specular, position;
			float intensity;

			light4->setPosition(cameraPosition);
			light4->setDirection(cameraDirection);
			if(!gameModelList.empty())
				for (auto model : gameModelList) {
					model.second->Draw(projection_matrix, view_matrix, cameraPosition);
				}
		}

		void Model_Manager::Update()
		{
			for (auto model : gameModelList) {
				model.second->Update();
			}
			for (auto model : gameModelList_NDC) {
				model.second->Update();
			}
		}

		void Model_Manager::DeleteModel(const String gameModelName)
		{
			IGameObject* model = gameModelList[gameModelName];
			model->Destroy();
			gameModelList.erase(gameModelName);
		}

		const IGameObject & Model_Manager::GetModel(const String gameModelName) const
		{
			return (*gameModelList.at(gameModelName));
		}

		void Model_Manager::DeleteModel_NDC(const String & gameModelName)
		{
			IGameObject* model = gameModelList_NDC[gameModelName];
			model->Destroy();
			gameModelList_NDC.erase(gameModelName);
		}

		const IGameObject & Model_Manager::GetModel_NDC(const String & gameModelName) const
		{
			return (*gameModelList_NDC.at(gameModelName));
		}

		void Model_Manager::SetModel(const String & gameObjectName, IGameObject * gameObject)
		{
			gameModelList[gameObjectName] = gameObject;
		}

		void Model_Manager::SetTextureToModel(const String & gameModelName, const String & textureName)
		{
		}

		void Model_Manager::setMaterialManager(Material_Manager *& material_m)
		{
			material_manager = material_m;
			StaticMesh* SMesh = new StaticMesh("Assets/Models/shuttle/shut.3ds");
			SMesh->SetProgram(Shader_Manager::GetShader("colorShader"));
			SMesh->setMaterialManager(material_manager);
			SMesh->loadFile("Assets/Models/nano/nanosuit.obj");			
			SMesh->setLights(light0);
			SMesh->setLights(light1);
			SMesh->setLights(light2);
			SMesh->setLights(light3);
			SMesh->setLights(light4);
			SMesh->SetRotation(0.0f, glm::vec3(0.01f, 0.01f, 0.01f));
			String name = "nano";
			gameModelList[name] = SMesh;
		}

	}

}
