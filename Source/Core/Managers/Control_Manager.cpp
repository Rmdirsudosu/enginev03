#include "Control_Manager.h"
#include "../Utils/Static/ConsoleInOut.h"

using Utils::ConsoleInOut;

namespace Engine {

	namespace Managers {

		Control_Manager::Control_Manager()
		{
			playerSpeed = 2.5f;
			gameStarted = false;
			playerCamera = new FPSCamera();
		}

		Control_Manager::~Control_Manager()
		{
		}

		void Control_Manager::notifyKeyPress(GLFWwindow * activeWin, float deltaTime)
		{
			playerSpeed = 2.5 * deltaTime;

			if (glfwGetKey(activeWin, GLFW_KEY_ESCAPE) == GLFW_PRESS)
				glfwSetWindowShouldClose(activeWin, true);
			if (glfwGetKey(activeWin, GLFW_KEY_SPACE) == GLFW_PRESS)
				ConsoleInOut::Out("YOU PRESS SPACE BAR");
			if (glfwGetKey(activeWin, GLFW_KEY_D) == GLFW_PRESS)
				playerCamera->straf(playerSpeed);
			if (glfwGetKey(activeWin, GLFW_KEY_A) == GLFW_PRESS)
				playerCamera->straf(-playerSpeed);
			if (glfwGetKey(activeWin, GLFW_KEY_W) == GLFW_PRESS)
				playerCamera->moveForward(playerSpeed);
			if (glfwGetKey(activeWin, GLFW_KEY_S) == GLFW_PRESS)
				playerCamera->moveForward(-playerSpeed);
			if (glfwGetKey(activeWin, GLFW_KEY_E) == GLFW_PRESS)
				playerCamera->moveUpward(playerSpeed);
			if (glfwGetKey(activeWin, GLFW_KEY_Q) == GLFW_PRESS)
				playerCamera->moveUpward(-playerSpeed);

		}

		void Control_Manager::notifyMouseInput(double xpos, double ypos, float deltaTime)
		{
			playerCamera->mouseInput(xpos, ypos, deltaTime);
		}

		void Control_Manager::notifyScrollInput(double xoffset, double yoffset, float deltaTime)
		{
			playerCamera->scrollInput(xoffset, yoffset, deltaTime);
		}

		void Control_Manager::notifyReshape(int width, int height)
		{
			playerCamera->setResolution(width, height);
		}

		void Control_Manager::notifyGameStart()
		{
			gameStarted = true;
			playerCamera->update();
		}

		void Control_Manager::update(glm::mat4 & view_matrix, glm::mat4 & projection_matrix, glm::vec3& cameraPosition, glm::vec3 &cameraDirection)
		{
			if (gameStarted) {
				playerCamera->getViewAndProjectionMatrix(view_matrix, projection_matrix);
				cameraPosition = playerCamera->getPlayerPosition();
				cameraDirection = playerCamera->getPlayerDirection();
			}
				
		}

		void Control_Manager::setPlayerPosition(glm::vec3 position, glm::vec3 rotation)
		{
			playerCamera->setPositionAndRotation(position, rotation);
		}

		void Control_Manager::getPlayerPosition(glm::vec3 & position, glm::vec3 & rotation)
		{
			playerCamera->getPlayerPositionAndRotation(position, rotation);
		}

		void Control_Manager::setPlayerSpeed(float speed)
		{
			playerSpeed = speed;
		}

	}

}
