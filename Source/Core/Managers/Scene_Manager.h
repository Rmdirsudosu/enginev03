#pragma once

#include "Shader_Manager.h"
#include "../Init/IListener.h"
#include "Model_Manager.h"
#include "Shader_Manager.h"
#include "File_Manager.h"
#include "Control_Manager.h"

namespace Engine {

	namespace Managers {

		class Scene_Manager : public Engine::Core::IListener {

		public:
			Scene_Manager();
			~Scene_Manager();

			virtual void notifyBeginFrame();
			virtual void notifyDisplayFrame();
			virtual void notifyEndFrame();
			virtual void notifyReshape(int width, int height, int previous_width, int previous_height);
			virtual void notifyViewMatrixChanges(glm::mat4 inViewMatrix);

			void setModelsManager(Model_Manager*& models_m);
			void setFileManager(File_Manager*& file_m);
			void setShaderManager(Shader_Manager*& shader_m);
			void setControlManager(Control_Manager*& control_m);

			void setSceneFile(String infile);

		private:
			File_Manager* file_manager;
			Model_Manager* model_manager;
			Shader_Manager* shader_manager;
			Control_Manager* control_manager;

			glm::mat4 projection_matrix;
			glm::mat4 view_matrix;
			glm::vec3 cameraPosition;
			glm::vec3 cameraDirection;

			Camera* activeCamera;


			String file;

			void loadShaders();

		};

	}

}

