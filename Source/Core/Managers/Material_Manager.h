#pragma once

#include "../Utils/Ressources/String.h"
#include "../Utils/Libs/stb_image.h"
#include <glad/glad.h>
#include <map>

using Utils::String;

namespace Engine {

	namespace Managers {

		//struct Texture {
		//	GLuint texture;
		//	GLsizei size;

		//	void setTexture(GLuint inTex) {
		//		texture = inTex;
		//	}
		//	void setSize(GLsizei inSize) {
		//		size = inSize;
		//	}
		//	GLuint getTexture() {
		//		return texture;
		//	}
		//	GLsizei getSize() {
		//		return size;
		//	}
		//};
	
		class Material_Manager {

		public:
			Material_Manager();
			~Material_Manager();

			static const GLuint getTexture(const String& textureName);

			void CreateTexture(const String& textureName ,const String& texturePath);

			void deleteTexture(const String& textureName);

		private:

			static std::map<String, GLuint> textures;

			static bool staticCheckIfNotExist(const String& textureName);

		};

	}

}