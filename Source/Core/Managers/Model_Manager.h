#pragma once

#include <map>
#include "Shader_Manager.h"
#include "../Rendering/IGameObject.h"
#include "../Rendering/Models/Triangle.h"
#include "../Rendering/Models/Quad.h"
#include "../Rendering/Models/CubeIndex.h"
#include "../Rendering/Models/StaticMesh.h"
#include "../Rendering/Ligh/Light.h"
#include "Material_Manager.h"

using namespace Engine::Rendering;
using Engine::Rendering::Lights::Light;

namespace Engine {

	namespace Managers {

		class Model_Manager {

		public:

			Model_Manager();
			~Model_Manager();

			void Draw();
			void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, glm::vec3& cameraPosition, glm::vec3& cameraDirection);
			void Update();
			void DeleteModel(const String gameModelName);
			const IGameObject& GetModel(const String gameModelName) const;

			void DeleteModel_NDC(const String& gameModelName);
			const IGameObject& GetModel_NDC(const String& gameModelName) const;

			void SetModel(const String& gameObjectName, IGameObject* gameObject);
			void SetTextureToModel(const String& gameModelName, const String& textureName);

			void setMaterialManager(Material_Manager*& material_m);

		private:
			std::map<String, IGameObject*> gameModelList;
			std::map<String, IGameObject*> gameModelList_NDC;
			Light* light0;
			Light* light1;
			Light* light2;
			Light* light3;
			Light* light4;

			Material_Manager* material_manager;
		};

	}

}

