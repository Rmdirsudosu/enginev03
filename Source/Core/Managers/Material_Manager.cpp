#include "Material_Manager.h"
#include "../Utils/Static/ConsoleInOut.h"

using Utils::ConsoleInOut;

namespace Engine {

	namespace Managers {

		std::map<String, GLuint> Material_Manager::textures;

		Material_Manager::Material_Manager()
		{
			CreateTexture("container", "Assets\\Textures\\container.jpg");
			CreateTexture("awesomeface", "Assets\\Textures\\awesomeface.png");
			CreateTexture("woodbox", "Assets\\Textures\\woodbox.jpg");
			CreateTexture("box", "Assets\\Textures\\container2.png");
			CreateTexture("spec", "Assets\\Textures\\container2_specular.png");
			CreateTexture("arm_dif.pngtexture_diffuse", "Assets/Models/nano/arm_dif.png");
			CreateTexture("body_dif.pngtexture_diffuse", "Assets/Models/nano/body_dif.png");
			CreateTexture("glass_dif.pngtexture_diffuse", "Assets/Models/nano/glass_dif.png");
			CreateTexture("hand_dif.pngtexture_diffuse", "Assets/Models/nano/hand_dif.png");
			CreateTexture("helmet_diff.pngtexture_diffuse", "Assets/Models/nano/helmet_diff.png");
			CreateTexture("leg_dif.pngtexture_diffuse", "Assets/Models/nano/leg_dif.png");
		}

		Material_Manager::~Material_Manager()
		{
			std::map<String, GLuint>::iterator i;
			for (i = textures.begin(); i != textures.end(); i++) {
				GLuint tex = i->second;
				GLsizei size = sizeof(tex);
				glDeleteTextures(size, &tex);
			}
			textures.clear();
		}

		const GLuint Material_Manager::getTexture(const String & textureName)
		{
			if (!staticCheckIfNotExist(textureName)) {
				return textures.at(textureName);
			}
			else
				ConsoleInOut::Out("TEXTURE DOES NOT EXIST: " + textureName);
			return 0;
		}

		void Material_Manager::CreateTexture(const String& textureName, const String & texturePath)
		{
			if (staticCheckIfNotExist(textureName)) {
				int width, height, nbrColor;
				unsigned char* data;

				//stbi_set_flip_vertically_on_load(true); // flip the image back to normal for openGL
				data = stbi_load(texturePath.toChars(), &width, &height, &nbrColor, 0);

				unsigned int texture;
				//glCreateTextures(GL_TEXTURE_2D, 1, &texture);
				glGenTextures(1, &texture);
				glBindTexture(GL_TEXTURE_2D, texture);

				if (data) {
					String Name = texturePath;
					GLenum format;
					if (nbrColor == 1)
						format = GL_RED;
					else if (nbrColor == 3)
						format = GL_RGB;
					else if (nbrColor == 4)
						format = GL_RGBA;

					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

					glTexStorage2D(GL_TEXTURE_2D, 1, format, width, height);
					glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
					glGenerateMipmap(GL_TEXTURE_2D);
					glBindTexture(GL_TEXTURE_2D, 0);


				}
				else {
					ConsoleInOut::Out("Fail to load texture: " + texturePath);
					stbi_image_free(data);
					return;
				}

				stbi_image_free(data);
				textures[textureName] = texture;
				// DO NOT DELETE TEXTURES !!!! 
				//glDeleteTextures(sizeof(texture), &texture);
			}
			else
				ConsoleInOut::Out("Texture " + textureName + " ALREADY EXIST.");

		}

		void Material_Manager::deleteTexture(const String & textureName)
		{
			if (staticCheckIfNotExist(textureName)) {
				GLuint tex = textures.at(textureName);
				glDeleteTextures(sizeof(tex), &tex);
				textures.erase(textureName);
			}
			else
				ConsoleInOut::Out("TEXTURES " + textureName + "DOES NOT EXIST AND CAN NOT BE DELETED.");
		}

		bool Material_Manager::staticCheckIfNotExist(const String & textureName)
		{
			std::map<String, GLuint>::iterator i = textures.find(textureName);
			if (i != textures.end())
				return false;
			else
				return true;
		}

	}

}
