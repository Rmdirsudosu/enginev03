#pragma once

#include "../Utils/Static/FileImporter.h"
//#include "../Utils/Static/FileExport.h"
#include <vector>
#include "../Utils/Ressources/String.h"

using Utils::String;
using Utils::FileImporter;

namespace Engine {

	namespace Managers {

		class File_Manager {

		public:
			static void getShaderInfos(String file, String& name, String& vertex, String& fragment);
			static void getSceneInfo(String file, String& name, String& width, String& height, bool isReshapeable);
			static void getTextureInfos(String file, String& name, String& location, String& size);
			static void getFile(String filename, String& file);

		private:

			static String subString(String Before, String After, String file);

		};

	}

}