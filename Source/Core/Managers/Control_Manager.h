#pragma once

#include "../Controls/IControl.h"
#include "../Controls/FPSCamera.h"
#include <glm/glm.hpp>

using Engine::Controls::IControl;
using Engine::Controls::Player::FPSCamera;

namespace Engine {

	namespace Managers {

		class Control_Manager : public IControl {

		public:
			Control_Manager();
			~Control_Manager();

			virtual void notifyKeyPress(GLFWwindow* activeWindow, float deltaTime);
			virtual void notifyMouseInput(double xpos, double ypos, float deltaTime);
			virtual void notifyScrollInput(double xoffset, double yoffset, float deltaTime);
			virtual void notifyReshape(int width, int height);
			virtual void notifyGameStart();

			void update(glm::mat4 &view_matrix, glm::mat4 &projection_matrix, glm::vec3& cameraPosition, glm::vec3 &cameraDirection);

			void setPlayerPosition(glm::vec3 position, glm::vec3 rotation);

			void getPlayerPosition(glm::vec3 &position, glm::vec3 &rotation);

			void setPlayerSpeed(float speed);

		private:
			FPSCamera* playerCamera;

			int oldWidth;
			int oldHeight;

			float playerSpeed;
			float runSpeed;

			bool gameStarted;

		};

	}

}