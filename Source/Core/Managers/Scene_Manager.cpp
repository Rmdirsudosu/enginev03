#include "Scene_Manager.h"
#include <vector>

namespace Engine {

	namespace Managers {

		Scene_Manager::Scene_Manager()
		{

			glEnable(GL_DEPTH_TEST);

			view_matrix = glm::mat4(1.0f, 0.0f, 0.0f, 0.0f,
				0.0f, 1.0f, 0.0f, 0.0f,
				0.0f, 0.0f, -1.0f, 0.0f,
				0.0f, 0.0f, 10.0f, 1.0f);

			//model_manager = new Model_Manager();
		}


		Scene_Manager::~Scene_Manager()
		{
			delete model_manager;
		}

		void Scene_Manager::notifyBeginFrame()
		{
			control_manager->update(view_matrix, projection_matrix, cameraPosition, cameraDirection);
			model_manager->Update();
		}

		void Scene_Manager::notifyDisplayFrame()
		{
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glClearColor(0.0, 0.0, 0.0, 1.0);

			model_manager->Draw();
			model_manager->Draw(projection_matrix, view_matrix, cameraPosition, cameraDirection);
		}

		void Scene_Manager::notifyEndFrame()
		{

		}

		void Scene_Manager::notifyReshape(int width, int height, int previous_width, int previous_height)
		{
			// NEED SOME NEW CODE HERE FOR GLFW !!!!!!!! 
			/*float ar = (float)glutGet(GLUT_WINDOW_WIDTH) /
				(float)glutGet(GLUT_WINDOW_HEIGHT);
			float angle = 45.0f, near1 = 0.1f, far1 = 2000.0f;

			projection_matrix[0][0] = 1.0f / (ar * tan(angle / 2.0f));
			projection_matrix[1][1] = 1.0f / tan(angle / 2.0f);
			projection_matrix[2][2] = (-near1 - far1) / (near1 - far1);
			projection_matrix[2][3] = 1.0f;
			projection_matrix[3][2] = 2.0f * near1 * far1 / (near1 - far1);*/
		}

		void Scene_Manager::notifyViewMatrixChanges(glm::mat4 inViewMatrix)
		{
			view_matrix = inViewMatrix;
		}

		void Scene_Manager::setModelsManager(Model_Manager *& models_m)
		{
			model_manager = models_m;
		}

		void Scene_Manager::setFileManager(File_Manager *& file_m)
		{
			file_manager = file_m;
		}

		void Scene_Manager::setShaderManager(Shader_Manager *& shader_m)
		{
			shader_manager = shader_m;
		}

		void Scene_Manager::setControlManager(Control_Manager *& control_m)
		{
			control_manager = control_m;
		}

		void Scene_Manager::setSceneFile(String infile)
		{
			//file_manager->getFile(infile, file);
			loadShaders();
		}

		void Scene_Manager::loadShaders()
		{
			// WILL BE UPDATE WITH FILE MANAGERS TO LOAD MULTIPLE SHADERS AT ONCE

			String name = "colorShader";
			String vertex = "Assets\\Shaders\\Vertex_Shader.glsl";
			String fragment = "Assets\\Shaders\\Fragment_Shader.glsl";
			// file_manager->getShaderInfos(file, name, vertex, fragment);

			shader_manager->CreateProgram(name, vertex, fragment);

			name = "lightShader";
			vertex = "Assets\\Shaders\\Light_Vertex_Shader.glsl";
			fragment = "Assets\\Shaders\\Light_Fragment_Shader.glsl";

			shader_manager->CreateProgram(name, vertex, fragment);
		}

	}

}
