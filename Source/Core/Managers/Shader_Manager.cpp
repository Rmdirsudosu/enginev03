#include "Shader_Manager.h"
#include "../Utils/Static/ConsoleInOut.h"

using Utils::ConsoleInOut;
using Utils::FileImporter;

namespace Engine {

	namespace Managers {

		std::map<String, GLuint> Shader_Manager::programs;

		Shader_Manager::Shader_Manager(void)
		{
		}

		Shader_Manager::~Shader_Manager(void)
		{
			std::map<String, GLuint>::iterator i;
			for (i = programs.begin(); i != programs.end(); i++) {
				GLuint pr = i->second;
				glDeleteProgram(pr);
			}
			programs.clear();
		}

		String Shader_Manager::ReadShader(const String& filename)
		{

			String shaderCode = FileImporter::getFile(G_GLSL, filename);

			return shaderCode;
		}

		GLuint Shader_Manager::CreateShader(GLenum shaderType, String source, String shaderName)
		{
			int compile_result = 0;

			GLuint shader = glCreateShader(shaderType);
			const char* shader_code_ptr = source.toChars();
			const int shader_code_size = source.size();

			glShaderSource(shader, 1, &shader_code_ptr, &shader_code_size);
			glCompileShader(shader);
			glGetShaderiv(shader, GL_COMPILE_STATUS, &compile_result);

			if (compile_result == GL_FALSE) {
				int info_log_length = 0;
				glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_log_length);
				std::vector<char> shader_log(info_log_length);
				glGetShaderInfoLog(shader, info_log_length, NULL, &shader_log[0]);
				std::cout << "ERROR COMPILING SHADER " << std::endl << &shader_log[0] << std::endl;
				return 0;
			}

			return shader;
		}

		void Shader_Manager::CreateProgram(const String& shaderName, const String& vertexShaderFilename, const String& fragmentShaderFilename)
		{
			if (checkIfNotExist(shaderName)) {
				//read the shader files and save the code
				String vertex_shader_code = ReadShader(vertexShaderFilename);
				String fragment_shader_code = ReadShader(fragmentShaderFilename);

				char v_shad[] = "vertex shader";
				char f_shad[] = "fragment shader";

				GLuint vertex_shader = CreateShader(GL_VERTEX_SHADER, vertex_shader_code, v_shad);
				GLuint fragment_shader = CreateShader(GL_FRAGMENT_SHADER, fragment_shader_code, f_shad);

				int link_result = 0;
				//create the program handle, attatch the shaders and link it
				GLuint program = glCreateProgram();

				// Attach Shader Vertex and Fragment to the program
				glAttachShader(program, vertex_shader);
				glAttachShader(program, fragment_shader);

				// Link the program
				glLinkProgram(program);

				// Check if the program was linked correctly
				glGetProgramiv(program, GL_LINK_STATUS, &link_result);
				//check for link errors and return errors in console
				if (link_result == GL_FALSE)
				{
					int info_log_length = 0;
					glGetProgramiv(program, GL_INFO_LOG_LENGTH, &info_log_length);
					std::vector<char> program_log(info_log_length);
					glGetProgramInfoLog(program, info_log_length, NULL, &program_log[0]);
					std::cout << "SHADER LINK ERROR" << std::endl << &program_log[0] << std::endl;
					return;
				}
				programs[shaderName] = program;
				glDeleteShader(vertex_shader);
				glDeleteShader(fragment_shader);
			}
			else ConsoleInOut::Out("Shader Already Exist");
		}
		const GLuint Shader_Manager::GetShader(const String & shaderName)
		{
			if (!staticCheckIfNotExist(shaderName))
				return programs.at(shaderName);
			else
				ConsoleInOut::Out("SHADER DOES NOT EXIST " + shaderName + "\n Impossible to get it ");
			return 0;
		}
		void Shader_Manager::deleteShader(const String & shaderName)
		{
			if (checkIfNotExist(shaderName)) {
				GLuint pr = programs.at(shaderName);
				glDeleteProgram(pr);
				programs.erase(shaderName);
			} else 
				ConsoleInOut::Out("SHADER DOES NOT EXIST " + shaderName + "\n Impossible to delete it ");
		}
		bool Shader_Manager::checkIfNotExist(const String & shaderName)
		{
			std::map<String, GLuint>::iterator i = programs.find(shaderName);
			if (i != programs.end())
				return false;
			else
				return true;
		}
		bool Shader_Manager::staticCheckIfNotExist(const String & shaderName)
		{
			std::map<String, GLuint>::iterator i = programs.find(shaderName);
			if (i != programs.end())
				return false;
			else
				return true;
		}
	}

}
