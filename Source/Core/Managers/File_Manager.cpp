#include "File_Manager.h"

using Utils::String;
using Utils::FileImporter;

namespace Engine {

	namespace Managers {
		void File_Manager::getShaderInfos(String file, String& name, String& vertex, String& fragment)
		{
			String Shader = subString("Shader{", "}", file);
			name = subString("name=", ";", Shader);
			vertex = subString("vertex=", ";", Shader);
			fragment = subString("fragment=", ";", Shader);
		}
		void File_Manager::getSceneInfo(String file, String& name, String& width, String& height, bool isReshapeable)
		{
			String Shader = subString("Scene{", "}", file);
			name = subString("name=", ";", Shader);
			width = subString("width=", ";", Shader);
			height = subString("height=", ";", Shader);
			isReshapeable = subString("isReshapeable=", ";", Shader).toBool();
		}
		void File_Manager::getTextureInfos(String file, String& name, String& location, String& size)
		{
			String Shader = subString("Texture{", "}", file);
			name = subString("name=", ";", Shader);
			location = subString("location=", ";", Shader);
			size = subString("size=", ";", Shader);
		}

		void File_Manager::getFile(String filename, String& file)
		{
			file = FileImporter::getFile(G_SCENES, filename);
			file.sanitize();
		}

		String File_Manager::subString(String before, String after, String file)
		{
			int Begin = file.index(before) + before.size();
			int End = file.subStr(Begin).index(after);
			String ret = file.subStr(Begin, End);
			return ret;
		}
	}

}
