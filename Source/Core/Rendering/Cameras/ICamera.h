#pragma once

#include <glm/glm.hpp>

namespace Engine {

	namespace Rendering {
	
		namespace Cameras {
		
			class ICamera {

			public:

				virtual ~ICamera() = 0;

				virtual void update() = 0;

				//virtual void notifyMatrixUpdate(glm::mat4 & in_view_matrix, glm::mat4 & in_projection_matrix) = 0;

			};

			inline ICamera::~ICamera() {
			

			}

		}

	}

}