#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "ICamera.h"

namespace Engine {

	namespace Rendering {
	
		namespace Cameras {
		
			class Camera {

			public:

				Camera();
				Camera(int inWidth, int inHeight);
				Camera(int inWidth, int inHeight, float inFoV);
				Camera(const Camera & inCamera);

				~Camera();

				void setResolution(int inWidth, int inHeight);
				void setFoV(float inFoV);
				void setRederingDitance(float inMaxRenderingDistance, float inMinRenderingDistance);

				void setPositionAndRotation(glm::vec3 inPosition, glm::vec3 inRotation);
				void setPosition(glm::vec3 inPosition);
				void setRotation(glm::vec3 inRotation);

				void getViewAndProjectionMatrix(glm::mat4 & in_view_matrix, glm::mat4 & in_projection_matrix);

				void switchOrthoPersp();

				virtual void update();

				void lookAt(glm::vec3 object);
				

			protected:
				glm::vec3 position;
				glm::vec3 rotation;
				glm::vec3 direction;

				glm::vec3 up;
				glm::vec3 right;

				glm::mat4 view;
				glm::mat4 projection;

				bool bOrthographic;

				int width;
				int height;
				float FoV;
				float minRenderingDistance;
				float maxRenderingDistance;

			};

		}

	}

}