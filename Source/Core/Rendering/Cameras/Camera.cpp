#include "Camera.h"

namespace Engine {

	namespace Rendering {
	
		namespace Cameras {
		


			Camera::Camera()
			{
				width = 1280;
				height = 720;
				position = glm::vec3(0.0, -3.0, 0.0);
				rotation = glm::vec3(0.0, 0.0, 0.0);
				direction = glm::vec3(0.0, 0.0, 0.0);
				bOrthographic = false;
				FoV = 45.0f;
				minRenderingDistance = 0.1f;
				maxRenderingDistance = 100.0f;
				up = glm::vec3(0.0, 1.0, 0.0);
			}

			Camera::Camera(int inWidth, int inHeight)
			{
				// TODO
			}

			Camera::Camera(int inWidth, int inHeight, float inFoV)
			{
				// TODO
			}

			Camera::Camera(const Camera & inCamera)
			{
				// TODO
			}

			Camera::~Camera()
			{
				// TODO
			}

			void Camera::setResolution(int inWidth, int inHeight)
			{
				width = inWidth;
				height = inHeight;
			}

			void Camera::setFoV(float inFoV)
			{
				FoV = inFoV;
			}

			void Camera::setRederingDitance(float inMaxRenderingDistance, float inMinRenderingDistance)
			{
				minRenderingDistance = inMinRenderingDistance;
				maxRenderingDistance = inMaxRenderingDistance;
			}

			void Camera::setPositionAndRotation(glm::vec3 inPosition, glm::vec3 inRotation)
			{
				position = inPosition;
				rotation = inRotation;
			}

			void Camera::setPosition(glm::vec3 inPosition)
			{
				position = inPosition;
			}

			void Camera::setRotation(glm::vec3 inRotation)
			{
				rotation = inRotation;
			}

			void Camera::getViewAndProjectionMatrix(glm::mat4 & in_view_matrix, glm::mat4 & in_projection_matrix)
			{
				in_view_matrix = view;
				in_projection_matrix = projection;
			}

			void Camera::switchOrthoPersp()
			{
				bOrthographic = !bOrthographic;
			}

			void Camera::update()
			{
				// TESTING CAMERA CLASS
				float radius = 10.0f;
				position.x = sin(glfwGetTime()) * radius;
				position.y = 0.0f;
				position.z = cos(glfwGetTime()) * radius;
				glm::mat4 view_matrix(1);
				view = glm::lookAt(position,
					direction, // Direction 
					up); //
				glm::mat4 projection_matrix(1);
				projection = glm::perspective(glm::radians(FoV), // Field of view
					(float)width / (float)height, // screen width and height in float
					minRenderingDistance, // start of the frustum
					maxRenderingDistance); // end of the frustum (to make it simple render distance)
			}

			void Camera::lookAt(glm::vec3 object)
			{
				direction = object;
			}

		}

	}

}
