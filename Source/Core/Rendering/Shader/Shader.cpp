#include "Shader.h"

namespace Engine {

	namespace Rendering {
	
		namespace Shaders {

			Shader::Shader()
			{
			}

			Shader::Shader(GLuint in_program)
			{
				program = in_program;
			}

			Shader::Shader(const Shader & in_shader)
			{
				program = in_shader.program;
			}

			Shader::~Shader()
			{
			}

			void Shader::setUniformBool(const String & name, bool value)
			{
				glUniform1i(glGetUniformLocation(program, name.toChars()), (int)value);
			}

			void Shader::setUniformInt(const String & name, int value)
			{
				glUniform1i(glGetUniformLocation(program, name.toChars()), value);
			}

			void Shader::setUniformFloat(const String & name, float value)
			{
				glUniform1f(glGetUniformLocation(program, name.toChars()), value);
			}

			void Shader::setUniformVec2(const String & name, glm::vec2 value)
			{
				glUniform2f(glGetUniformLocation(program, name.toChars()), value.x, value.y);
			}

			void Shader::setUniformVec3(const String & name, glm::vec3 value)
			{
				glUniform3f(glGetUniformLocation(program, name.toChars()), value.x, value.y, value.z);
			}

			void Shader::setUniformVec4(const String & name, glm::vec4 value)
			{
				glUniform4f(glGetUniformLocation(program, name.toChars()), value.x, value.y, value.z, value.w);
			}

			void Shader::setUniformMat2(const String & name, glm::mat2 value)
			{
				glUniformMatrix2fv(glGetUniformLocation(program, name.toChars()), 1, GL_FALSE, &value[0][0]);
			}

			void Shader::setUniformMat3(const String & name, glm::mat3 value)
			{
				glUniformMatrix3fv(glGetUniformLocation(program, name.toChars()), 1, GL_FALSE, &value[0][0]);
			}

			void Shader::setUniformMat4(const String & name, glm::mat4 value)
			{
				glUniformMatrix4fv(glGetUniformLocation(program, name.toChars()), 1, GL_FALSE, &value[0][0]);
			}

			void Shader::use()
			{
				glUseProgram(program);
			}

			void Shader::setProgram(GLuint in_program)
			{
				program = in_program;
			}

			GLuint Shader::getProgram()
			{
				return program;
			}

		}

	}

}