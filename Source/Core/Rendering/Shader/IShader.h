#pragma once

#include "../../Utils/Ressources/String.h"
#include <glm/glm.hpp>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

using Utils::String;

namespace Engine {

	namespace Rendering {
	
		class IShader {

		public:

			virtual ~IShader() = 0;

			virtual void setUniformBool(const String & name, bool value) = 0;
			virtual void setUniformInt(const String & name, int value) = 0;
			virtual void setUniformFloat(const String & name, float value) = 0;
			virtual void setUniformVec2(const String & name, glm::vec2 value) = 0;
			virtual void setUniformVec3(const String & name, glm::vec3 value) = 0;
			virtual void setUniformVec4(const String & name, glm::vec4 value) = 0;
			virtual void setUniformMat2(const String & name, glm::mat2 value) = 0;
			virtual void setUniformMat3(const String & name, glm::mat3 value) = 0;
			virtual void setUniformMat4(const String & name, glm::mat4 value) = 0;

			virtual void use() = 0;

			virtual void setProgram(GLuint in_program) = 0;
			virtual GLuint getProgram() = 0;

		};

		inline IShader::~IShader() {

		}	
	}

}