#pragma once

#include <glm/glm.hpp>
#include "IShader.h"

using Engine::Rendering::IShader;

namespace Engine {

	namespace Rendering {
	
		namespace Shaders {

			class Shader : public IShader {

			public:

				Shader();
				Shader(GLuint in_program);
				Shader(const Shader& in_shader);
				~Shader();

				virtual void setUniformBool(const String & name, bool value) override;
				virtual void setUniformInt(const String & name, int value) override;
				virtual void setUniformFloat(const String & name, float value) override;
				virtual void setUniformVec2(const String & name, glm::vec2 value) override;
				virtual void setUniformVec3(const String & name, glm::vec3 value) override;
				virtual void setUniformVec4(const String & name, glm::vec4 value) override;
				virtual void setUniformMat2(const String & name, glm::mat2 value) override;
				virtual void setUniformMat3(const String & name, glm::mat3 value) override;
				virtual void setUniformMat4(const String & name, glm::mat4 value) override;

				virtual void use() override;

				virtual void setProgram(GLuint in_program) override;
				virtual GLuint getProgram() override;

			private:
				GLuint program;

			};

		}

	}

}