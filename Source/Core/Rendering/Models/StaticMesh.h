#pragma once

#include "Mesh.h"
#include "Model.h"
#include <vector>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include "../../Utils/Libs/stb_image.h"
#include "../../Managers/Material_Manager.h"

using Utils::String;
using Engine::Managers::Material_Manager;

namespace Engine {

	namespace Rendering {
	
		namespace Models {
		
			class StaticMesh : public Model {			

			public:

				StaticMesh();
				StaticMesh(String filePath);

				~StaticMesh();

				void Create();
				virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, glm::vec3& cameraPosition) override final;
				virtual void Draw() override final;
				virtual void Update() override final;

				void loadFile(String filePath);

				virtual void SetRotation(float inRotationAxis, glm::vec3 inRotationSpeed) override final;
				virtual void SetPosition(glm::vec3 inPosition) override final;
				virtual void SetScale(glm::vec3 inScale) override final;

				virtual void setColor(glm::vec3 inColor) override final;

				virtual void setLights(Light *&in_light) override final;

				void setMaterialManager(Material_Manager* m_mat);

				void testModelsTextures(String s, GLuint text);

			private:
				std::vector<Mesh> meshes;
				String filePath;
				std::map<String, GLuint> textures_loaded;
				std::vector<String> texture_load;

				Material_Manager* m_material_manager;

				void loadModel(String path);
				void processNode(aiNode* node, const aiScene* scene);
				Mesh processMesh(aiMesh* mesh, const aiScene* scene);
				void loadMaterialTextures(aiMaterial* mat, aiTextureType type, String name, std::map<String, GLuint> textureMap);
				void TextureFromFile(const char *path, const String &directory, bool gamma);

			};

		}

	}

}