#include "Quad.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace Engine {

	namespace Rendering {

		namespace Models {



			Quad::Quad()
			{
			}

			Quad::~Quad()
			{
			}

			void Quad::Create()
			{
				GLuint vao;
				GLuint vbo;
				GLuint ibo;

				glGenVertexArrays(1, &vao);
				glBindVertexArray(vao);

				std::vector<VertexFormat>vertices;
				vertices.push_back(VertexFormat(glm::vec3(0.5, 0.5, 0.0), glm::vec4(1, 0, 0, 1), glm::vec2(1, 1)));
				vertices.push_back(VertexFormat(glm::vec3(0.5, -0.5, 0.0), glm::vec4(0, 1, 0, 1), glm::vec2(1, 0)));
				vertices.push_back(VertexFormat(glm::vec3(-0.5, -0.5, 0.0), glm::vec4(0, 0, 1, 1), glm::vec2(0, 0)));
				vertices.push_back(VertexFormat(glm::vec3(-0.5, 0.5, 0.0), glm::vec4(1, 1, 1, 1), glm::vec2(0, 1)));

				std::vector<unsigned int> indices = { 0, 1, 3,
										  1, 2, 3 };

				glGenBuffers(1, &vbo);
				glBindBuffer(GL_ARRAY_BUFFER, vbo);
				glBufferData(GL_ARRAY_BUFFER, 
					sizeof(VertexFormat) * 4, 
					&vertices[0], 
					GL_STATIC_DRAW);

				glGenBuffers(1, &ibo);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER,
					indices.size() * sizeof(unsigned int),
					&indices[0],
					GL_STATIC_DRAW);

				glEnableVertexAttribArray(0);
				glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)0);

				glEnableVertexAttribArray(1);
				glVertexAttribPointer(1, // Attrib array number
					4, // number of values for each vertices here 4 (RGBA)
					GL_FLOAT, // Value type
					GL_FALSE, // Should we normalise
					sizeof(VertexFormat), // number of byte before the next vertices in the buffer
					(void*)offsetof(VertexFormat, VertexFormat::color)); // offset here we say that he should start reading at the Color value

				glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(VertexFormat),
					(void*)(offsetof(VertexFormat, VertexFormat::texture)));
				glEnableVertexAttribArray(2);

				this->vao = vao;
				this->vbos.push_back(vbo);
				this->vbos.push_back(ibo);

			}

			void Quad::Draw()
			{
				float timeValue = glfwGetTime();
				float greenValue = (sin(timeValue) / 2) + 0.5f;
				
				// Get a uniform value from a shader
				int vertexColorLocation = glGetUniformLocation(program, "uniColor"); 

				

				glUseProgram(program);
				// TESTS WITH MATRIX
				//glm::mat4 trans(1); // declare a identity matrix
				//trans = glm::scale(trans, glm::vec3(0.5, 0.5, 0.5));
				//trans = glm::translate(trans, glm::vec3(0.5, -0.5, 0.0f));
				//// rotation must be after translation, if we want to rotate on the center axis.
				//trans = glm::rotate(trans, // Transformation matrix
				//	(float)glfwGetTime(),  // Angle in degree
				//	glm::vec3(0.0, 0.0, 1.0)); // witch axis to rotate around X, Y, Z

				//glUniformMatrix4fv(glGetUniformLocation(program, "transform"), 1, GL_FALSE, &trans[0][0]);

				// END TEST MATRIX

				// MODEL MATRIX from Local space to world space
				glm::mat4 model_matrix(1);
				model_matrix = glm::rotate(model_matrix, // Transformation matrix
					glm::radians(-55.0f), // Angle in degree
					glm::vec3(1.0f, 0.0f, 0.0f)); // witch axis to rotate around X, Y, Z
				glUniformMatrix4fv(glGetUniformLocation(program, "model_matrix"), 1, GL_FALSE, &model_matrix[0][0]);

				// VIEW MATRIX from world space to camera space
				glm::mat4 view_matrix(1);
				view_matrix = glm::translate(view_matrix, 
					glm::vec3(0.0f, 0.0f, -3.0f)); // axis of translation X, Y, Z
				glUniformMatrix4fv(glGetUniformLocation(program, "view_matrix"), 1, GL_FALSE, &view_matrix[0][0]);


				// PROJECTION MATRIX from camera space to clip space
				glm::mat4 projection_matrix(1);
				projection_matrix = glm::perspective(glm::radians(45.0f), // Field of view
					(float)1280 / (float)720, // screen width and height in float
					0.1f, // start of the frustum
					100.0f); // end of the frustum (to make it simple render distance)
				glUniformMatrix4fv(glGetUniformLocation(program, "projection_matrix"), 1, GL_FALSE, &projection_matrix[0][0]);


				glActiveTexture(GL_TEXTURE0); //active the texture unit 0 we can active the texture unit 1, ...  
				glBindTexture(GL_TEXTURE_2D, this->GetTexture("container")); // Bind the texture to that texture unit
				glActiveTexture(GL_TEXTURE1);
				glBindTexture(GL_TEXTURE_2D, this->GetTexture("awesomeface"));

				
				glUniform1i(glGetUniformLocation(program, "texture0"), 0);
				glUniform1i(glGetUniformLocation(program, "texture1"), 1);

				

				// link a value to a uniform value
				glUniform4f(vertexColorLocation, 
					0.0f, // value 1 here Blue because OpenGL work with BGR Blue Green Red
					greenValue, // value 2 here green
					0.0f, // value 3 here red
					1.0f); // Value 4 alpha
				/*
				f: the function expects a float as its value
				i: the function expects an int as its value
				ui: the function expects an unsigned int as its value
				3f: the function expects 3 floats as its value
				fv: the function expects a float vector/array as its value
				*/

				

				glBindVertexArray(vao);
				glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

				// Default draw mode draw a polygone with its face filled
				//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);				
				
				// draw a wiremesh polygone
				//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
				
			}

			void Quad::Update()
			{
			}

		}

	}

}

