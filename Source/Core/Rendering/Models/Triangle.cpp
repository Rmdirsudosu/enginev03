#include "Triangle.h"

using namespace Engine::Rendering::Models;

Triangle::Triangle()
{

}

Triangle::~Triangle()
{
}

void Triangle::Create()
{
	GLuint vao;
	GLuint vbo;

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	std::vector<VertexFormat> vertices;
	vertices.push_back(VertexFormat(glm::vec3(-0.5, -0.5, 0.0), glm::vec4(1.0, 0.0, 0.0, 1.0), glm::vec2(0, 0)));
	vertices.push_back(VertexFormat(glm::vec3(0.5, -0.5, 0.0), glm::vec4(0.0, 1.0, 0.0, 1.0), glm::vec2(1, 0)));
	vertices.push_back(VertexFormat(glm::vec3(0.0, 0.5, 0.0), glm::vec4(0.0, 0.0, 1.0, 1.0), glm::vec2(0.5, 1)));

	// Param 1 = how many buffer object do we want (the size)
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	// GL_STATIC_DRAW : Does not change or rarely change, GL_DYNAMIC_DRAW : is likely to change a lot, GL_STREAM_DRAW : change at ever call
	glBufferData(GL_ARRAY_BUFFER, sizeof(VertexFormat) * 3, &vertices[0], GL_STATIC_DRAW);
	
	glVertexAttribPointer(0, // Vertex Attrib array number (see line 31)
		3, // the number of values for the vertex attribute here it's a vec3 so 3 values.
		GL_FLOAT, // type of data here it's a float
		GL_FALSE, // If the data should be normalised (for int and Byte if necessary)
		sizeof(VertexFormat), // the size between the begining of this vertex attribute and the next one.
		(void*)0); // the offset of the value into the buffer.
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), 
		(void*)(offsetof(VertexFormat, VertexFormat::color))); // Here we've an offset, because VertexFormat does have
	//other values than color, so the offset is where the color is into a vertex format. more detail below
	glEnableVertexAttribArray(1); // Enable the attrib array set before.
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(VertexFormat),
		(void*)(offsetof(VertexFormat, VertexFormat::texture)));
	glEnableVertexAttribArray(2); 

	/*The first parameter specifies which vertex attribute we want to configure. Remember that we specified the location of 
	the position vertex attribute in the vertex shader with layout (location = 0). This sets the location of the vertex attribute 
	to 0 and since we want to pass data to this vertex attribute, we pass in 0.

	The next argument specifies the size of the vertex attribute. The vertex attribute is a vec3 so it is composed of 3 values.
	
	The third argument specifies the type of the data which is GL_FLOAT (a vec* in GLSL consists of floating point values).
	
	The next argument specifies if we want the data to be normalized. If we're inputting integer data types (int, byte) and 
	we've set this to GL_TRUE, the integer data is normalized to 0 (or -1 for signed data) and 1 when converted to float.
	This is not relevant for us so we'll leave this at GL_FALSE.

	The fifth argument is known as the stride and tells us the space between consecutive vertex attributes. Since the next
	set of position data is located exactly 3 times the size of a float away we specify that value as the stride. Note that 
	since we know that the array is tightly packed (there is no space between the next vertex attribute value) we could've 
	also specified the stride as 0 to let OpenGL determine the stride (this only works when values are tightly packed).
	Whenever we have more vertex attributes we have to carefully define the spacing between each vertex attribute but 
	we'll get to see more examples of that later on.

	The last parameter is of type void* and thus requires that weird cast. This is the offset of where the position data 
	begins in the buffer. Since the position data is at the start of the data array this value is just 0. We will explore 
	this parameter in more detail later on*/

	this->vao = vao;
	this->vbos.push_back(vbo);

}

void Triangle::Update()
{
}

void Triangle::Draw()
{
	glActiveTexture(GL_TEXTURE0); //active the texture unit 0 we can active the texture unit 1, ...  
	glBindTexture(GL_TEXTURE_2D, this->GetTexture("container")); // Bind the texture to that texture unit
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, this->GetTexture("awesomeface"));

	glUseProgram(program); // WE MUST USE the program before 
	glUniform1i(glGetUniformLocation(program, "texture0"), 0);// texture chanel 0
	glUniform1i(glGetUniformLocation(program, "texture1"), 1);// texture chanel 1

	
	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES, 0, 3);
}
