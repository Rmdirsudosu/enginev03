#include "Object.h"
#include "../ObjectLoader.h"

#define PI 3.14159265

namespace Engine {

	namespace Rendering {

		namespace Models {

			Objects::Objects()
			{
			}

			Objects::~Objects()
			{
			}

			void Objects::Create()
			{
				GLuint vao;
				GLuint vbo;
				GLuint ibo;
				GLuint nbo;

				time(&timer);

				glGenVertexArrays(1, &vao);
				glBindVertexArray(vao);

				std::vector<VertexFormat> vertices;
				std::vector<glm::vec3> normals;
				std::vector<unsigned int>  indices;
				Rendering::ObjectLoader::LoadOBJ("Assets\\Objects\\CubeTest.obj", vertices, normals, indices, false);

				vertexNumber = indices.size();

				glGenBuffers(1, &vbo);
				glBindBuffer(GL_ARRAY_BUFFER, vbo);
				glBufferData(GL_ARRAY_BUFFER,
					vertices.size() * sizeof(VertexFormat),
					&vertices[0],
					GL_STATIC_DRAW);

				glGenBuffers(1, &nbo);
				glBindBuffer(GL_ARRAY_BUFFER, nbo);
				glBufferData(GL_ARRAY_BUFFER,
					normals.size() * sizeof(glm::vec3),
					&normals[0],
					GL_STATIC_DRAW);

				glGenBuffers(1, &ibo);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER,
					indices.size() * sizeof(unsigned int),
					&indices[0],
					GL_STATIC_DRAW);

				glBindBuffer(GL_ARRAY_BUFFER, vbo);
				glEnableVertexAttribArray(0);
				glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)0);

				glEnableVertexAttribArray(1);
				glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE,
					sizeof(VertexFormat),
					(void*)(offsetof(VertexFormat, VertexFormat::texture)));

				glBindBuffer(GL_ARRAY_BUFFER, nbo);
				glEnableVertexAttribArray(2);
				glVertexAttribPointer(
					2,                                // attribute
					3,                                // size
					GL_FLOAT,                         // type
					GL_FALSE,                         // normalized?
					0,                                // stride
					(void*)0                          // array buffer offset
				);

				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

				glBindVertexArray(0);



				this->vao = vao;
				this->vbos.push_back(vbo);
				this->vbos.push_back(nbo);
				this->vbos.push_back(ibo);


				rotation_speed = glm::vec3(0.0, 0.0, 0.0);
				rotation = glm::vec3(0.005, 0.05, 2.0);
			}

			void Objects::Draw(const glm::mat4 & projection_matrix, const glm::mat4 & view_matrix, glm::vec3& cameraPosition)
			{
				rotation = 0.01f * rotation_speed + rotation;

				glm::vec3 rotation_sin = glm::vec3(rotation.x * PI / 180, rotation.y * PI / 180, rotation.z * PI / 180);

				glUseProgram(program);
				glUniform3f(glGetUniformLocation(program, "rotation"),
					rotation_sin.x,
					rotation_sin.y,
					rotation_sin.z);
				glUniformMatrix4fv(glGetUniformLocation(program, "view_matrix"), 1,
					false, &view_matrix[0][0]);
				glUniformMatrix4fv(glGetUniformLocation(program, "projection_matrix"), 1, false, &projection_matrix[0][0]);
				glBindVertexArray(vao);
				glDrawElements(GL_TRIANGLES, vertexNumber, GL_UNSIGNED_INT, 0);
			}

			void Objects::Update()
			{
				rotation = 0.01f * rotation_speed + rotation;
				rotation_sin = glm::vec3(rotation.x * PI / 180, rotation.y * PI / 180, rotation.z * PI / 180);
			}

		}

	}

}