#pragma once

#include <vector>
#include <map>
#include "../IGameObject.h"
#include "../Ligh/ILight.h"
#include "../Shader/Shader.h"

using Engine::Rendering::Lights::ILight;
using Engine::Rendering::Shaders::Shader;

namespace Engine {

	namespace Rendering {

		namespace Models {

			struct Material {

				glm::vec3 ambiant;
				glm::vec3 diffuse;
				glm::vec3 specular;
				float shininess;

			};

			struct Texture {

				GLuint texture;
				String type;
				String path;

			};

			class Model : public IGameObject {

			public:
				Model();
				virtual ~Model();

				virtual void Draw() override;
				virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, glm::vec3& cameraPosition) override;
				virtual void Update() override;
				virtual void SetProgram(GLuint shaderProgram) override;
				virtual void SetTexture(GLuint texture, String & textureName) override;
				virtual void Destroy() override;

				virtual GLuint GetVao() const override;
				virtual const std::vector<GLuint>& GetVbos() const override;

				virtual GLuint GetTexture(String textureName) override;
				virtual void SetTexture(String textureName, GLuint texture) override;

				virtual void SetRotation(float inRotationAxis, glm::vec3 inRotationSpeed) = 0;
				virtual void SetPosition(glm::vec3 inPosition) = 0;
				virtual void SetScale(glm::vec3 scale) = 0;

				virtual void setColor(glm::vec3 inColor) = 0;

				virtual void setLights(Light *&in_light) override;

				virtual void setAmbiant(glm::vec3 inAmbiant);
				virtual void setDiffuse(glm::vec3 inDiffuse);
				virtual void setSpecular(glm::vec3 inSpecular);
				virtual void setShininess(float inShininess);

			protected:
				GLuint vao;
				GLuint program;
				Shader shader;

				std::vector<GLuint> vbos;
				std::map<String, GLuint> textures;

				glm::vec3 position;
				glm::vec3 rotationSpeedOnAxis;
				glm::vec3 rotationAxis;
				glm::vec3 scale;
				glm::vec3 color;

				Material material;

				std::vector<ILight*> lights;

				float lightIntensity;
				glm::vec3 lightPosition;
				glm::vec3 lightColor;

			};

		}

	}

}