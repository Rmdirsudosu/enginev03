#pragma once

#include "Model.h"
#include "../VertexFormat.h"
#include <time.h>
#include <stdarg.h>
#include "../Ligh/ILight.h"

using namespace Engine::Rendering::Models;
using Utils::String;
using namespace Engine::Rendering::Lights;

class CubeIndex : public Model {

public:
	CubeIndex();
	~CubeIndex();

	void Create();
	virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, glm::vec3& cameraPosition) override final;
	virtual void Draw() override final;
	virtual void Update() override final;

	virtual void SetRotation(float inRotationAxis, glm::vec3 inRotationSpeed) override final;
	virtual void SetPosition(glm::vec3 inPosition) override final;
	virtual void SetScale(glm::vec3 inScale) override final;

	virtual void setColor(glm::vec3 inColor) override final;


private:
	glm::vec3 rotation, rotation_speed;
	glm::vec3 rotation_sin;
	time_t timer;

};