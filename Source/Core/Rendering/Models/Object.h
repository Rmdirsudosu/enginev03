#pragma once

#include "Model.h"
#include <time.h>
#include <stdarg.h>

namespace Engine {

	namespace Rendering {
		namespace Models {

			class Objects : public Model {

			public:
				Objects();
				~Objects();

				void Create();
				virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, glm::vec3& cameraPosition) override final;
				virtual void Update() override final;

			private:
				glm::vec3 rotation, rotation_speed;
				glm::vec3 rotation_sin;
				time_t timer;

				int vertexNumber;
			};

		}

	}

}