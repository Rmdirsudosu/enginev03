#include "StaticMesh.h"
#include <glm/gtc/matrix_transform.hpp>

namespace Engine {

	namespace Rendering {

		namespace Models {



			StaticMesh::StaticMesh()
			{
			}

			StaticMesh::StaticMesh(String filePath)
			{
			}

			StaticMesh::~StaticMesh()
			{
			}

			void StaticMesh::Create()
			{
			}

			void StaticMesh::Draw(const glm::mat4 & projection_matrix, const glm::mat4 & view_matrix, glm::vec3 & cameraPosition)
			{
				glm::mat4 model_matrix(1);
				model_matrix = glm::scale(model_matrix, glm::vec3(0.2f));
				//model_matrix = glm::translate(model_matrix, position);
				//model_matrix = glm::rotate(model_matrix, // Transformation matrix
				//	glm::radians(rotationAxis.x), // Angle in degree 
				//	rotationSpeedOnAxis); // witch axis to rotate around X, Y, Z CANT BE EQUALS TO 0.0f 0.0f 0.0f 
				for (unsigned int i = 0; i < meshes.size(); i++) {
					meshes[i].Draw(projection_matrix, view_matrix, model_matrix, cameraPosition);
				}
			}

			void StaticMesh::Draw()
			{
			}

			void StaticMesh::Update()
			{
			}

			void StaticMesh::loadFile(String filePath)
			{
				loadModel(filePath);
			}

			void StaticMesh::SetRotation(float inRotationAxis, glm::vec3 inRotationSpeed)
			{
			}

			void StaticMesh::SetPosition(glm::vec3 inPosition)
			{
			}

			void StaticMesh::SetScale(glm::vec3 inScale)
			{
			}

			void StaticMesh::setColor(glm::vec3 inColor)
			{
			}

			void StaticMesh::setLights(Light *& in_light)
			{
				for (unsigned int i = 0; i < meshes.size(); i++) {
					meshes[i].setLights(in_light);
				}
			}

			void StaticMesh::setMaterialManager(Material_Manager * m_mat)
			{
				//m_material_manager = m_mat;
			}

			void StaticMesh::testModelsTextures(String s, GLuint text)
			{
				/*for (unsigned int i = 0; i < meshes.size(); i++) {
					meshes[i].SetTexture(s, text);
				}*/
			}

			void StaticMesh::loadModel(String path)
			{
				Assimp::Importer import;
				const aiScene *scene = import.ReadFile(path.toChars(), aiProcess_Triangulate | aiProcess_FlipUVs);

				if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
					String error = import.GetErrorString();
					ConsoleInOut::Out("ERROR ASSIMP " +error);
					return;
				}
				std::string s = path.toChars();
				filePath = s.substr(0, s.find_last_of('/')).c_str();

				processNode(scene->mRootNode, scene);
			}

			void StaticMesh::processNode(aiNode * node, const aiScene * scene)
			{
				for (unsigned int i = 0; i < node->mNumMeshes; i++)
				{
					aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
					meshes.push_back(processMesh(mesh, scene));
					for (int i = 0; i < texture_load.size(); i++) {
						GLuint test = Material_Manager::getTexture(texture_load[i]);
						String s = texture_load[i];
						meshes.back().SetTexture(texture_load[i], Material_Manager::getTexture(texture_load[i]));
					}
					texture_load.clear();
				}
				// then do the same for each of its children
				for (unsigned int i = 0; i < node->mNumChildren; i++)
				{
					processNode(node->mChildren[i], scene);
				}
			}

			Mesh StaticMesh::processMesh(aiMesh * mesh, const aiScene * scene)
			{
				std::vector<VertexFormat> vertices;
				std::vector<unsigned int> indices;
				std::map<String, GLuint> textu;

				for (unsigned int i = 0; i < mesh->mNumVertices; i++) {
					VertexFormat vertex;
					glm::vec3 vector;
					
					// position
					vector.x = mesh->mVertices[i].x;
					vector.y = mesh->mVertices[i].y;
					vector.z = mesh->mVertices[i].z;
					vertex.position = vector;
					// normals
					vector.x = mesh->mNormals[i].x;
					vector.y = mesh->mNormals[i].y;
					vector.z = mesh->mNormals[i].z;
					vertex.normals = vector;

					if (mesh->mTextureCoords[0]) // does the mesh contain texture coordinates?
					{
						glm::vec2 vec;
						// a vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't 
						// use models where a vertex can have multiple texture coordinates so we always take the first set (0).
						vec.x = mesh->mTextureCoords[0][i].x;
						vec.y = mesh->mTextureCoords[0][i].y;
						vertex.texture = vec;
					}
					else
						vertex.texture = glm::vec2(0.0f, 0.0f);

					// tangent
					/*vector.x = mesh->mTangents[i].x;
					vector.y = mesh->mTangents[i].y;
					vector.z = mesh->mTangents[i].z;
					vertex.tangent = vector;*/
					// bitangent
					/*vector.x = mesh->mBitangents[i].x;
					vector.y = mesh->mBitangents[i].y;
					vector.z = mesh->mBitangents[i].z;
					vertex.bitangent = vector;*/
					vertices.push_back(vertex);
				}
				for (unsigned int i = 0; i < mesh->mNumFaces; i++)
				{
					aiFace face = mesh->mFaces[i];
					// retrieve all indices of the face and store them in the indices vector
					for (unsigned int j = 0; j < face.mNumIndices; j++)
						indices.push_back(face.mIndices[j]);
				}

				aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];

				loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse", textu);
				//loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular", textu);

				

				return Mesh(vertices, indices, program);
			}

			void StaticMesh::loadMaterialTextures(aiMaterial * mat, aiTextureType type, String name, std::map<String, GLuint> textureMap)
			{
				for (int i = 0; i < mat->GetTextureCount(type); i++)
				{
					aiString str;
					mat->GetTexture(type, i, &str);
					// check if texture was loaded before and if so, continue to next iteration: skip loading a new texture
					String filename = String(str.C_Str());
					String path = this->filePath + '/' + filename;
					filename = filename + name;
					//m_material_manager->CreateTexture(filename, path);
					texture_load.push_back(filename);
					//SetTexture(filename, m_material_manager->getTexture(filename));
				}
			}


			// NOT IN USE FOR THE MOMENT  !! 
			void StaticMesh::TextureFromFile(const char * path, const String & directory, bool gamma)
			{
				String filename = String(path);
				filename = directory + '/' + filename;				

				int width, height, nrComponents;
				unsigned char *data;
				data = stbi_load(filename.toChars(), &width, &height, &nrComponents, 0);
				if (data)
				{
					GLenum format;
					if (nrComponents == 1)
						format = GL_RED;
					else if (nrComponents == 3)
						format = GL_RGB;
					else if (nrComponents == 4)
						format = GL_RGBA;

					unsigned int textureID;
					glGenTextures(1, &textureID);
					glBindTexture(GL_TEXTURE_2D, textureID);

					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

					glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
					glGenerateMipmap(GL_TEXTURE_2D);

					stbi_image_free(data);
					String mapName = path + String("diffuse");
					textures_loaded[mapName] = textureID;
				}
				else
				{
					std::cout << "Texture failed to load at path: " << path << std::endl;
					stbi_image_free(data);
				}
			}

		}

	}

}