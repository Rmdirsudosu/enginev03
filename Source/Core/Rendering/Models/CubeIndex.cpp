#include "CubeIndex.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

using Engine::Rendering::VertexFormat;

CubeIndex::CubeIndex()
{
	position = glm::vec3(0.0f, 0.0f, 0.0f);
	rotationAxis = glm::vec3(0.0f, 0.0f, 0.0f);
	rotationSpeedOnAxis = glm::vec3(0.01f, 0.01f, 0.01f);
	scale = glm::vec3(1.0f);
	lightColor = glm::vec3(1.0f);
	lightIntensity = 1.0f;
	lightPosition = glm::vec3(0.0f);
	color = glm::vec3(1.0f, 0.5f, 0.31f);
	material.ambiant = glm::vec3(1.0f, 0.5f, 0.31f);
	material.diffuse = glm::vec3(1.0f, 0.5f, 0.31f);
	material.specular = glm::vec3(0.5f, 0.5f, 0.5f);
	material.shininess = 32.0f;
}

CubeIndex::~CubeIndex()
{
}

void CubeIndex::Create()
{
	GLuint vao;
	GLuint vbo;
	GLuint ibo;

	time(&timer);

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	std::vector<VertexFormat> vertices;
	std::vector<unsigned int>  indices = { 0,  1,  2,  0,  2,  3,   //front
						   4,  5,  6,  4,  6,  7,   //right
						   8,  9,  10, 8,  10, 11 ,  //back
						   12, 13, 14, 12, 14, 15,  //left
						   16, 17, 18, 16, 18, 19,  //upper
						   20, 21, 22, 20, 22, 23 }; //bottom

	//back
	vertices.push_back(VertexFormat(glm::vec3(-0.5f, -0.5f, -0.5f),
		glm::vec2(0.0f, 0.0f), glm::vec3(0.0f, 0.0f, -1.0f)));
	vertices.push_back(VertexFormat(glm::vec3(0.5f, -0.5f, -0.5f),
		glm::vec2(1.0f, 0.0f), glm::vec3(0.0f, 0.0f, -1.0f)));
	vertices.push_back(VertexFormat(glm::vec3(0.5f, 0.5f, -0.5f),
		glm::vec2(1.0f, 1.0f), glm::vec3(0.0f, 0.0f, -1.0f)));
	vertices.push_back(VertexFormat(glm::vec3(-0.5f, 0.5f, -0.5f),
		glm::vec2(0.0f, 1.0f), glm::vec3(0.0f, 0.0f, -1.0f)));

	//front
	vertices.push_back(VertexFormat(glm::vec3(-0.5, -0.5, 0.5), // Vertix Coor
		glm::vec2(0.0f, 0.0f),// UV Coor
		glm::vec3(0.0f, 0.0f, 1.0f))); // normal
	vertices.push_back(VertexFormat(glm::vec3(0.5, -0.5, 0.5),
		glm::vec2(1.0f, 0.0f),
		glm::vec3(0.0f, 0.0f, 1.0f)));
	vertices.push_back(VertexFormat(glm::vec3(0.5, 0.5, 0.5),
		glm::vec2(1.0f, 1.0f),
		glm::vec3(0.0f, 0.0f, 1.0f)));
	vertices.push_back(VertexFormat(glm::vec3(-0.5f, 0.5f, 0.5f),
		glm::vec2(0.0f, 1.0f), glm::vec3(0.0f, 0.0f, 1.0f)));

	//left
	vertices.push_back(VertexFormat(glm::vec3(-0.5f, -0.5f, -0.5f),
		glm::vec2(0.0f, 1.0f), glm::vec3(-1.0f, 0.0f, 0.0f)));
	vertices.push_back(VertexFormat(glm::vec3(-0.5f, -0.5f, 0.5f),
		glm::vec2(0.0f, 0.0f), glm::vec3(-1.0f, 0.0f, 0.0f)));
	vertices.push_back(VertexFormat(glm::vec3(-0.5f, 0.5f, 0.5f),
		glm::vec2(1.0f, 0.0f), glm::vec3(-1.0f, 0.0f, 0.0f)));
	vertices.push_back(VertexFormat(glm::vec3(-0.5f, 0.5f, -0.5f),
		glm::vec2(1.0f, 1.0f), glm::vec3(-1.0f, 0.0f, 0.0f)));

	//right
	vertices.push_back(VertexFormat(glm::vec3(0.5f, 0.5f, 0.5f),
		glm::vec2(1.0f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f)));
	vertices.push_back(VertexFormat(glm::vec3(0.5f, 0.5f, -0.5f),
		glm::vec2(1.0f, 1.0f), glm::vec3(1.0f, 0.0f, 0.0f)));
	vertices.push_back(VertexFormat(glm::vec3(0.5f, -0.5f, -0.5f),
		glm::vec2(0.0f, 1.0f), glm::vec3(1.0f, 0.0f, 0.0f)));
	vertices.push_back(VertexFormat(glm::vec3(0.5f, -0.5f, 0.5f),
		glm::vec2(0.0f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f)));

	////bottom
	vertices.push_back(VertexFormat(glm::vec3(-0.5f, -0.5f, -0.5f),
		glm::vec2(0.0f, 1.0f), glm::vec3(0.0f, -1.0f, 0.0f)));
	vertices.push_back(VertexFormat(glm::vec3(0.5f, -0.5f, -0.5f),
		glm::vec2(1.0f, 1.0f), glm::vec3(0.0f, -1.0f, 0.0f)));
	vertices.push_back(VertexFormat(glm::vec3(0.5f, -0.5f, 0.5f),
		glm::vec2(1.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f)));
	vertices.push_back(VertexFormat(glm::vec3(-0.5f, -0.5f, 0.5f),
		glm::vec2(0.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f)));

	//upper
	vertices.push_back(VertexFormat(glm::vec3(0.5f, 0.5f, 0.5f),
		glm::vec2(1.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f)));
	vertices.push_back(VertexFormat(glm::vec3(-0.5f, 0.5f, 0.5f),
		glm::vec2(0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f)));
	vertices.push_back(VertexFormat(glm::vec3(-0.5f, 0.5f, -0.5f),
		glm::vec2(0.0f, 1.0f), glm::vec3(0.0f, 1.0f, 0.0f)));
	vertices.push_back(VertexFormat(glm::vec3(0.5f, 0.5f, -0.5f),
		glm::vec2(1.0f, 1.0f), glm::vec3(0.0f, 1.0f, 0.0f)));

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER,
		vertices.size() * sizeof(VertexFormat),
		&vertices[0],
		GL_STATIC_DRAW);

	glGenBuffers(1, &ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		indices.size() * sizeof(unsigned int),
		&indices[0],
		GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE,
		sizeof(VertexFormat),
		(void*)(offsetof(VertexFormat, VertexFormat::normals)));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE,
		sizeof(VertexFormat),
		(void*)(offsetof(VertexFormat, VertexFormat::texture)));
	glBindVertexArray(0);

	this->vao = vao;
	this->vbos.push_back(vbo);
	this->vbos.push_back(ibo);

	rotation_speed = glm::vec3(90.0, 90.0, 90.0);
	rotation = glm::vec3(0.0, 0.0, 0.0);
}

void CubeIndex::Draw(const glm::mat4 & projection_matrix, const glm::mat4 & view_matrix, glm::vec3& cameraPosition)
{
	float timeValue = glfwGetTime();
	float greenValue = (sin(timeValue) / 2) + 0.5f;

	// Get a uniform value from a shader
	int vertexColorLocation = glGetUniformLocation(program, "uniColor");

	glUseProgram(program);

	// Light settings To optimize !!!! 
	//if (!lights.empty()) {
	//	for (int i = 0; i < lights.size(); i++) {
	//		if (i == 0) {
	//			// Set la directional light en s�par�e !!!
	//			// TODO
	//			glm::vec3 direction;
	//			glm::vec3 ambient;
	//			glm::vec3 diffuse;
	//			glm::vec3 specular;
	//			float intensity;
	//			lights[i]->getLightProperties(direction, ambient, diffuse, specular, intensity);
	//			glUniform3f(glGetUniformLocation(program, "DLight.direction"), direction.x, direction.y, direction.z);
	//			glUniform3f(glGetUniformLocation(program, "DLight.ambient"), ambient.x, ambient.y, ambient.z);
	//			glUniform3f(glGetUniformLocation(program, "DLight.diffuse"), diffuse.x, diffuse.y, diffuse.z);
	//			glUniform3f(glGetUniformLocation(program, "DLight.specular"), specular.x, specular.y, specular.z);
	//		}
	//		else {
	//			String name = "lights[" + String::toString(i - 1) + "]";
	//			glm::vec3 position;
	//			glm::vec3 direction;
	//			glm::vec3 ambient;
	//			glm::vec3 diffuse;
	//			glm::vec3 specular;
	//			float intensity;
	//			float constant;
	//			float linear;
	//			float quadratic;
	//			float innerCutoff;
	//			float outerCutoff;

	//			lights[i]->getLightProperties(position, ambient, diffuse, specular, direction, constant, linear, quadratic, innerCutoff, outerCutoff, intensity);

	//			name = "lights[0].position";
	//			name.replace(String::toString(i - 1), "0");
	//			glUniform3f(glGetUniformLocation(program, name.toChars()), position.x, position.y, position.z);
	//			name = "lights[0].direction";
	//			name.replace(String::toString(i - 1), "0");
	//			glUniform3f(glGetUniformLocation(program, name.toChars()), direction.x, direction.y, direction.z);
	//			name = "lights[0].ambient";
	//			name.replace(String::toString(i - 1), "0");
	//			glUniform3f(glGetUniformLocation(program, name.toChars()), ambient.x, ambient.y, ambient.z);
	//			name = "lights[0].diffuse";
	//			name.replace(String::toString(i - 1), "0");
	//			glUniform3f(glGetUniformLocation(program, name.toChars()), diffuse.x, diffuse.y, diffuse.z);
	//			name = "lights[0].specular";
	//			name.replace(String::toString(i - 1), "0");
	//			glUniform3f(glGetUniformLocation(program, name.toChars()), specular.x, specular.y, specular.z);
	//			name = "lights[0].constant";
	//			name.replace(String::toString(i - 1), "0");
	//			glUniform1f(glGetUniformLocation(program, name.toChars()), constant);
	//			name = "lights[0].linear";
	//			name.replace(String::toString(i - 1), "0");
	//			glUniform1f(glGetUniformLocation(program, name.toChars()), linear);
	//			name = "lights[0].quadratic";
	//			name.replace(String::toString(i - 1), "0");
	//			glUniform1f(glGetUniformLocation(program, name.toChars()), quadratic);
	//			name = "lights[0].type";
	//			name.replace(String::toString(i - 1), "0");
	//			//glUniform1i(glad_glGetUniformLocation(program, name.toChars() + *".intensity"), material.shininess);
	//			glUniform1i(glGetUniformLocation(program, name.toChars()), lights[i]->getType());
	//			name = "lights[0].innerCutoff";
	//			name.replace(String::toString(i - 1), "0");
	//			glUniform1f(glGetUniformLocation(program, name.toChars()), glm::cos(glm::radians(innerCutoff)));
	//			name = "lights[0].outerCutoff";
	//			name.replace(String::toString(i - 1), "0");
	//			glUniform1f(glGetUniformLocation(program, name.toChars()), glm::cos(glm::radians(outerCutoff)));
	//		}
	//	}
	//}

	// Materials settings
	/*glUniform3f(glad_glGetUniformLocation(program, "material.ambiant"), material.ambiant.r, material.ambiant.g, material.ambiant.b);
	glUniform3f(glad_glGetUniformLocation(program, "material.diffuse"), material.diffuse.r, material.diffuse.g, material.diffuse.b);
	glUniform3f(glad_glGetUniformLocation(program, "material.specular"), material.specular.r, material.specular.g, material.specular.b);*/
	glUniform1f(glad_glGetUniformLocation(program, "material.shininess"), material.shininess);

	// Object color
	glUniform3f(glGetUniformLocation(program, "globalColor"), color.r, color.g, color.b);

	glm::mat4 model_matrix(1);
	model_matrix = glm::translate(model_matrix, position);
	model_matrix = glm::rotate(model_matrix, // Transformation matrix
		glm::radians(rotationAxis.x), // Angle in degree 
		rotationSpeedOnAxis); // witch axis to rotate around X, Y, Z CANT BE EQUALS TO 0.0f 0.0f 0.0f 
	glUniformMatrix4fv(glGetUniformLocation(program, "model_matrix"), 1, GL_FALSE, &model_matrix[0][0]);

	// CAMERA POSITION 
	glUniform3f(glGetUniformLocation(program, "viewPosition"), cameraPosition.x, cameraPosition.y, cameraPosition.z);	

	//VIEW MATRIX
	glUniformMatrix4fv(glGetUniformLocation(program, "view_matrix"), 1, GL_FALSE, &view_matrix[0][0]);
	// PROJECTION MATRIX
	glUniformMatrix4fv(glGetUniformLocation(program, "projection_matrix"), 1, GL_FALSE, &projection_matrix[0][0]);

	glActiveTexture(GL_TEXTURE0); //active the texture unit 0 we can active the texture unit 1, ...  
	glBindTexture(GL_TEXTURE_2D, this->GetTexture("box")); // Bind the texture to that texture unit
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, this->GetTexture("spec"));


	glUniform1i(glGetUniformLocation(program, "texture_diffuse1"), 0);
	glUniform1i(glGetUniformLocation(program, "texture_diffuse1"), 0);
	glUniform1i(glGetUniformLocation(program, "texture_specular1"), 1);



	// link a value to a uniform value
	glUniform4f(vertexColorLocation,
		0.0f, // value 1 here Blue because OpenGL work with BGR Blue Green Red
		greenValue, // value 2 here green
		0.0f, // value 3 here red
		1.0f); // Value 4 alpha



	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
	glActiveTexture(GL_TEXTURE0);

}

void CubeIndex::Draw()
{
	float timeValue = glfwGetTime();
	float greenValue = (sin(timeValue) / 2) + 0.5f;

	// Get a uniform value from a shader
	int vertexColorLocation = glGetUniformLocation(program, "globalColor");



	glUseProgram(program);
	// TESTS WITH MATRIX
	//glm::mat4 trans(1); // declare a identity matrix
	//trans = glm::scale(trans, glm::vec3(0.5, 0.5, 0.5));
	//trans = glm::translate(trans, glm::vec3(0.5, -0.5, 0.0f));
	//// rotation must be after translation, if we want to rotate on the center axis.
	//trans = glm::rotate(trans, // Transformation matrix
	//	(float)glfwGetTime(),  // Angle in degree
	//	glm::vec3(0.0, 0.0, 1.0)); // witch axis to rotate around X, Y, Z

	//glUniformMatrix4fv(glGetUniformLocation(program, "transform"), 1, GL_FALSE, &trans[0][0]);

	// END TEST MATRIX

	// MODEL MATRIX from Local space to world space
	glm::mat4 model_matrix(1);
	model_matrix = glm::scale(model_matrix, scale);
	model_matrix = glm::translate(model_matrix, position);
	model_matrix = glm::rotate(model_matrix, // Transformation matrix
		glm::radians(rotationAxis.x), // Angle in degree 
		rotationSpeedOnAxis); // witch axis to rotate around X, Y, Z CANT BE EQUALS TO 0.0f 0.0f 0.0f 
	glUniformMatrix4fv(glGetUniformLocation(program, "model_matrix"), 1, GL_FALSE, &model_matrix[0][0]);

	// VIEW MATRIX from world space to camera space
	glm::mat4 view_matrix(1);
	view_matrix = glm::translate(view_matrix,
		glm::vec3(0.0f, 0.0f, -3.0f)); // axis of translation X, Y, Z
	glUniformMatrix4fv(glGetUniformLocation(program, "view_matrix"), 1, GL_FALSE, &view_matrix[0][0]);


	// PROJECTION MATRIX from camera space to clip space
	glm::mat4 projection_matrix(1);
	projection_matrix = glm::perspective(glm::radians(45.0f), // Field of view
		(float)1280 / (float)720, // screen width and height in float
		0.1f, // start of the frustum
		100.0f); // end of the frustum (to make it simple render distance)
	glUniformMatrix4fv(glGetUniformLocation(program, "projection_matrix"), 1, GL_FALSE, &projection_matrix[0][0]);

	glActiveTexture(GL_TEXTURE0); //active the texture unit 0 we can active the texture unit 1, ...  
	glBindTexture(GL_TEXTURE_2D, this->GetTexture("container")); // Bind the texture to that texture unit
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, this->GetTexture("awesomeface"));


	glUniform1i(glGetUniformLocation(program, "texture_diffuse1"), 0);
	glUniform1i(glGetUniformLocation(program, "texture_specular1"), 1);



	// link a value to a uniform value
	glUniform3f(vertexColorLocation,
		color.r, color.b, color.b); // Value 4 alpha
	/*
	f: the function expects a float as its value
	i: the function expects an int as its value
	ui: the function expects an unsigned int as its value
	3f: the function expects 3 floats as its value
	fv: the function expects a float vector/array as its value
	*/



	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);

	// Default draw mode draw a polygone with its face filled
	//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);				

	// draw a wiremesh polygone
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}

void CubeIndex::Update()
{
}

void CubeIndex::SetRotation(float inRotationAxis, glm::vec3 inRotationSpeed)
{
	rotationAxis.x = inRotationAxis;
	rotationSpeedOnAxis = inRotationSpeed;
}

void CubeIndex::SetPosition(glm::vec3 inPosition)
{
	position = inPosition;
}

void CubeIndex::SetScale(glm::vec3 inScale)
{
	scale = inScale;
}

void CubeIndex::setColor(glm::vec3 inColor)
{
	color = inColor;
}
