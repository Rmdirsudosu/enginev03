#include "Model.h"

namespace Engine {

	namespace Rendering {

		namespace Models {

			Model::Model() {

			}

			Model::~Model() {
				Destroy();
			}

			void Model::Draw()
			{
			}

			void Model::Draw(const glm::mat4 & projection_matrix, const glm::mat4 & view_matrix, glm::vec3& cameraPosition)
			{
			}

			void Model::Update()
			{
			}

			void Model::SetProgram(GLuint shaderProgram)
			{
				program = shaderProgram;
			}

			void Model::SetTexture(GLuint const texture, String & textureName)
			{
				std::map<String, GLuint>::iterator i = textures.find(textureName);
				if (i != textures.end()) {
					textures[textureName] = texture;
					
				}
				else
					ConsoleInOut::Out("TEXTURE ALREADY LINK TO THE OBJECT.");
			}

			void Model::Destroy()
			{
				glDeleteVertexArrays(1, &vao);
				glDeleteBuffers(vbos.size(), &vbos[0]);
				vbos.clear();

				if (textures.size() > 0)
				{
					for (auto t : textures)
					{
						glDeleteTextures(1, &t.second);
					}
					textures.clear();
				}
			}

			GLuint Model::GetVao() const
			{
				return vao;
			}

			const std::vector<GLuint>& Model::GetVbos() const
			{
				return vbos;
			}

			GLuint Model::GetTexture(String textureName)
			{
				if(!textures.empty())
					return textures.at(textureName);
				else ConsoleInOut::Out("TEXTURE IS NOT LINK TO THE MODEL");
			}

			void Model::SetTexture(String textureName, GLuint texture)
			{
				if (texture == 0) return;
				textures[textureName] = texture;
			}

			void Model::setLights(Light *&in_light)
			{
				lights.push_back(in_light);
			}

			void Model::setAmbiant(glm::vec3 inAmbiant)
			{
				material.ambiant = inAmbiant;
			}

			void Model::setDiffuse(glm::vec3 inDiffuse)
			{
				material.diffuse = inDiffuse;
			}

			void Model::setSpecular(glm::vec3 inSpecular)
			{
				material.specular = inSpecular;
			}

			void Model::setShininess(float inShininess)
			{
				material.shininess = inShininess;
			}

		}

	}

}
