#include "Mesh.h"
#include "../../Managers/Material_Manager.h"

using namespace Engine::Rendering::Models;
using Engine::Managers::Material_Manager;

Mesh::Mesh(std::vector<VertexFormat> in_vertices, std::vector<unsigned int> in_indices, GLuint in_program)
{
	vertices = in_vertices;
	indices = in_indices;
	program = in_program;
	shader.setProgram(program);
	Create();
}

Engine::Rendering::Models::Mesh::Mesh(const Mesh & mesh)
{
	vertices = mesh.vertices;
	indices = mesh.indices;
	program = mesh.program;
	textures = mesh.textures;
	shader.setProgram(program);
	Create();
}

Mesh::~Mesh()
{
}

void Mesh::Create()
{
	GLuint vao;
	GLuint vbo;
	GLuint ibo;

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER,
		vertices.size() * sizeof(VertexFormat),
		&vertices[0],
		GL_STATIC_DRAW);

	glGenBuffers(1, &ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		indices.size() * sizeof(unsigned int),
		&indices[0],
		GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE,
		sizeof(VertexFormat),
		(void*)(offsetof(VertexFormat, VertexFormat::normals)));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE,
		sizeof(VertexFormat),
		(void*)(offsetof(VertexFormat, VertexFormat::texture)));
	glBindVertexArray(0);

	this->vao = vao;
	this->vbos.push_back(vbo);
	this->vbos.push_back(ibo);
}

void Mesh::Draw(const glm::mat4 & projection_matrix, const glm::mat4 & view_matrix, glm::vec3 & cameraPosition)
{
	
}

void Mesh::Draw(const glm::mat4 & projection_matrix, const glm::mat4 & view_matrix, const glm::mat4 & model_matrix, glm::vec3 & cameraPosition)
{
	int diffuseNr = 1;
	int specularNr = 1;

	shader.use();
	unsigned int i = 0;
	if (!textures.empty()) {
		for (auto tex: textures) {
			glActiveTexture(GL_TEXTURE0 + i); // active proper texture unit before binding
			glBindTexture(GL_TEXTURE_2D, this->GetTexture(tex.first));
		 // retrieve texture number (the N in diffuse_textureN)
			String number;
			String name = tex.first;
			String bufferName;
			if (name.contains("diffuse")) {
				number = String::toString(diffuseNr++);
				bufferName = "texture_diffuse1";
			}
			else if (name.contains("specular")) {
				number = String::toString(specularNr++); // transfer unsigned int to stream
				bufferName = "texture_specular1";
			}
			//else if (name == "texture_normal")
			//	number = std::to_string(normalNr++); // transfer unsigned int to stream
			//else if (name == "texture_height")
			//	number = std::to_string(heightNr++); // transfer unsigned int to stream

													 // now set the sampler to the correct texture unit
			glUniform1i(glGetUniformLocation(program, bufferName.toChars()), i);
			// and finally bind the texture
			
			i++;
		}

		glUniform1f(glGetUniformLocation(program, "material.shininess"), 32.0f);

		glUniformMatrix4fv(glGetUniformLocation(program, "model_matrix"), 1, GL_FALSE, &model_matrix[0][0]);

		// CAMERA POSITION 
		glUniform3f(glGetUniformLocation(program, "viewPosition"), cameraPosition.x, cameraPosition.y, cameraPosition.z);

		//VIEW MATRIX
		glUniformMatrix4fv(glGetUniformLocation(program, "view_matrix"), 1, GL_FALSE, &view_matrix[0][0]);
		// PROJECTION MATRIX
		glUniformMatrix4fv(glGetUniformLocation(program, "projection_matrix"), 1, GL_FALSE, &projection_matrix[0][0]);

		glBindVertexArray(vao);
		glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);

		glActiveTexture(GL_TEXTURE0);

	}
}

void Mesh::Update()
{
	
}

void Mesh::SetShader(Shader shaderProgram)
{
	programUse = &shaderProgram;
}

void Engine::Rendering::Models::Mesh::SetRotation(float inRotationAxis, glm::vec3 inRotationSpeed)
{
}

void Engine::Rendering::Models::Mesh::SetPosition(glm::vec3 inPosition)
{
}

void Engine::Rendering::Models::Mesh::SetScale(glm::vec3 inScale)
{
}

void Engine::Rendering::Models::Mesh::setColor(glm::vec3 inColor)
{
}

void Engine::Rendering::Models::Mesh::setText(String s, GLuint i)
{
	if (i == 0) return;
	text.push_back(i);
}
