#pragma once

#include <glm/glm.hpp>
#include "../../Utils/Ressources/String.h"
#include "../VertexFormat.h"
#include <vector>
#include <map>
#include "Model.h"

using Utils::String;
using Engine::Rendering::VertexFormat;

namespace Engine {

	namespace Rendering {
	
		namespace Models {

			class Mesh : public Model {

			public:
				Mesh(std::vector<VertexFormat> in_vertices, std::vector<unsigned int> in_indeices, std::map<String, GLuint> in_textures);
				Mesh(std::vector<VertexFormat> in_vertices, std::vector<unsigned int> in_indeices, GLuint program);
				Mesh(const Mesh & mesh);
				~Mesh();

				void Create();
				virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, glm::vec3& cameraPosition) override final;
				virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, const glm::mat4& model_matrix, glm::vec3& cameraPosition);
				virtual void Update() override final;
				virtual void SetShader(Shader shaderProgram);

				virtual void SetRotation(float inRotationAxis, glm::vec3 inRotationSpeed) override final;
				virtual void SetPosition(glm::vec3 inPosition) override final;
				virtual void SetScale(glm::vec3 inScale) override final;
				virtual void setColor(glm::vec3 inColor) override final;

				void setText(String s, GLuint i);

			private:
				std::vector<VertexFormat> vertices;
				std::vector<unsigned int> indices;

				std::vector<GLuint> text;

				Shader* programUse;

			};

		}

	}

}