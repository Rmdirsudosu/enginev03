#pragma once

#include <glm/glm.hpp>

namespace Engine{

	namespace Rendering {

		struct VertexFormat {

			glm::vec3 position;
			glm::vec4 color;
			glm::vec3 normals;
			glm::vec2 texture;
			glm::vec3 tangent;
			glm::vec3 bitangent;

			//COPY CONSTRUCTOR			
			VertexFormat(const glm::vec3 &inPos, const glm::vec4 &inColor) {
				position = inPos;
				color = inColor;
				texture.x = texture.y = 0;
				normals.x = normals.y = normals.z = 0;
			}

			VertexFormat(const glm::vec3 &inPos, const glm::vec4 &inColor, const glm::vec2 inTexture) {
				position = inPos;
				color = inColor;
				texture = inTexture;
				normals.x = normals.y = normals.z = 0;
			}

			VertexFormat(const glm::vec3 &inPos, const glm::vec2 &inTexture)
			{
				position = inPos;
				texture = inTexture;
				color.r = color.g = color.b = color.a = 0;
				normals.x = normals.y = normals.z = 0;
			}

			VertexFormat(const glm::vec3 &inPos, const glm::vec2 &inTexture, const glm::vec3 &inNormal)
			{
				position = inPos;
				texture = inTexture;
				normals = inNormal;
				color.r = color.g = color.b = color.a = 0;
			}

			VertexFormat(const glm::vec3 &inPos, const glm::vec2 &inTexture, const glm::vec3 &inNormals, glm::vec4 inColor)
			{
				position = inPos;
				texture = inTexture;
				normals = inNormals;
				color = inColor;
			}

			VertexFormat() {}

		};
	}

}