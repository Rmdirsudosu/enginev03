#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <vector>
#include "VertexFormat.h"
#include "../Utils/Static/ConsoleInOut.h"
#include "../Rendering/Ligh/Light.h"

using Utils::String;
using Utils::ConsoleInOut;
using Engine::Rendering::Lights::Light;

namespace Engine {

	namespace Rendering {

		class IGameObject {

		public:
			virtual ~IGameObject() = 0;

			virtual void Draw() = 0;
			virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrice, glm::vec3& cameraPosition) = 0;
			virtual void Update() = 0;
			virtual void SetProgram(GLuint shaderProgram) = 0;
			virtual void SetTexture(GLuint texture, String & textureName) = 0;
			virtual void Destroy() = 0;

			virtual GLuint GetVao() const = 0;
			virtual const std::vector<GLuint>& GetVbos() const = 0;

			virtual void SetTexture(String textureName, GLuint texture) = 0;
			virtual GLuint GetTexture(String textureName) = 0;
			
			virtual void setLights(Light *&in_light) = 0;

		};

		inline IGameObject::~IGameObject() {

		}

	}

}