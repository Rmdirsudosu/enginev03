#include "ObjectLoader.h"
#include "../Utils/Static/ConsoleInOut.h"
#include <stdio.h>
#include <string>
#include "../Utils/Ressources/String.h"
#include <cstring>
#include "VBOIndexer.h"

using Utils::ConsoleInOut;

namespace Engine {

	namespace Rendering {

		bool ObjectLoader::LoadOBJ(const char * filename,
			std::vector<VertexFormat>& out_vertices,
			std::vector<glm::vec3>& out_normals,
			std::vector<unsigned int>& out_indices,
			bool DDS)
		{
			ConsoleInOut::Out("Loading object file : ", filename);

			std::vector<unsigned int> vertexIndices, uvIndices, normalIndices;
			std::vector<glm::vec3> temp_vertices;
			std::vector<glm::vec2> temp_uvs;
			std::vector<glm::vec3> temp_normals;

			// Open the file in "r" = read mode
			FILE* file = fopen(filename, "r");
			if (file == NULL) {
				ConsoleInOut::Out("Can't open file: ", filename);
				return false;
			}

			while (1) {

				char lineHeader[128];

				int res = fscanf(file, "%s", lineHeader);
				if (res == EOF) {
					break;
				}

				if (strcmp(lineHeader, "v") == 0) {
					glm::vec3 vertex;
					fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
					temp_vertices.push_back(vertex);
				}
				else if (strcmp(lineHeader, "vt") == 0) {
					glm::vec2 uvs;
					fscanf(file, "%f %f\n", &uvs.x, &uvs.y);
					if (DDS) // If Texture = DDS Y axis is inverted => DDS = DirectX
						uvs.y = -uvs.y;
					temp_uvs.push_back(uvs);
				}
				else if (strcmp(lineHeader, "vn") == 0) {
					glm::vec3 normal;
					fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
					temp_normals.push_back(normal);
				}
				else if (strcmp(lineHeader, "f") == 0) {
					unsigned int vIndex[3], uvIndex[3], nIndex[3];
					int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n",
						&vIndex[0],
						&uvIndex[0],
						&nIndex[0],
						&vIndex[1],
						&uvIndex[1],
						&nIndex[1],
						&vIndex[2],
						&uvIndex[2],
						&nIndex[2]);
					if (matches != 9) {
						ConsoleInOut::Out("WRONG FILE FORMAT:", filename);
						fclose(file);
						return false;
					}
					vertexIndices.push_back(vIndex[0]);
					vertexIndices.push_back(vIndex[1]);
					vertexIndices.push_back(vIndex[2]);
					uvIndices.push_back(uvIndex[0]);
					uvIndices.push_back(uvIndex[1]);
					uvIndices.push_back(uvIndex[2]);
					normalIndices.push_back(nIndex[0]);
					normalIndices.push_back(nIndex[1]);
					normalIndices.push_back(nIndex[2]);
				}
				else {
					char nextLineBuffer[1000];
					fgets(nextLineBuffer, 1000, file);
				}

			}

			std::vector<VertexFormat> in_vertices;
			std::vector<glm::vec3> in_normals;

			for (unsigned int i = 0; i < vertexIndices.size(); i++) {
				unsigned int vertexIndex = vertexIndices[i];
				unsigned int uvIndex = uvIndices[i];
				unsigned int normalIndex = normalIndices[i];

				glm::vec3 vertex = temp_vertices[vertexIndex - 1]; // -1 because an array begin at 0 
				glm::vec2 uv = temp_uvs[uvIndex - 1];
				glm::vec3 normal = temp_normals[normalIndex - 1];

				in_vertices.push_back(VertexFormat(vertex, uv));
				in_normals.push_back(normal);
			}

			VBOIndexer::IndexVBO(in_vertices, in_normals, out_indices, out_vertices, out_normals);

			ConsoleInOut::Out("Object File Load ", filename);
			fclose(file);
			return true;
		}

	}

}