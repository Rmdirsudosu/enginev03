#include "VBOIndexer.h"

namespace Engine {

	namespace Rendering {



		void VBOIndexer::IndexVBO(std::vector<VertexFormat>& in_vertices,
			std::vector<glm::vec3>& in_normals,
			std::vector<unsigned int>& out_indices,
			std::vector<VertexFormat>& out_vertices,
			std::vector<glm::vec3>& out_normals)
		{
			std::map<PackedVertex, unsigned int> VertexToOutIndex;

			// CHeck every input vertex
			for (unsigned int i = 0; i < in_vertices.size(); i++) {

				PackedVertex packed = { in_vertices[i], in_normals[i] };

				unsigned int index;

				bool found = getSimilarVertexIndex(packed, VertexToOutIndex, index);

				if (found) {
					out_indices.push_back(index);
				}
				else {
					out_vertices.push_back(in_vertices[i]);
					out_normals.push_back(in_normals[i]);
					unsigned int newIndex = (unsigned int)out_vertices.size() - 1;
					out_indices.push_back(newIndex);
					VertexToOutIndex[packed] = newIndex;
				}

			}
		}

		bool VBOIndexer::getSimilarVertexIndex(PackedVertex & packed, std::map<PackedVertex, unsigned int>& VertexToOutIndex, unsigned int & result)
		{
			std::map<PackedVertex, unsigned int>::iterator it = VertexToOutIndex.find(packed);
			// we check that the Iterator is not the end of VertexToOutIndex
			if (it == VertexToOutIndex.end()) {
				return false;
			}
			else {
				result = it->second; // If it is not the end of VertexToOutIndex then the index of that vertex is the index (second) of it.
				return true;
			}
		}

	}

}
