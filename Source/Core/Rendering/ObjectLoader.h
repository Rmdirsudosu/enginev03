#pragma once

#include <vector>
#include "glm/glm.hpp"
#include "VertexFormat.h"

namespace Engine {

	namespace Rendering {

		class ObjectLoader {

		public:
			static bool LoadOBJ(const char* filename,
				std::vector<VertexFormat> & out_vertices,
				std::vector<glm::vec3> & out_normals,
				std::vector<unsigned int>&  out_indices,
				bool DDS);

		};

	}

}