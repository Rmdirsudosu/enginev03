#pragma once

#include <vector>
#include "glm/glm.hpp"
#include <map>
#include "VertexFormat.h"

namespace Engine {

	namespace Rendering {

		struct PackedVertex {

			VertexFormat positionAndUV;
			glm::vec3 normal;

			bool operator<(const PackedVertex that) const {
				//Compare two memory block
				return memcmp((void*)this, (void*)&that, sizeof(PackedVertex)) > 0;
			};

		};

		class VBOIndexer {

		public:
			static void IndexVBO(std::vector<VertexFormat> & in_vertices,
				std::vector<glm::vec3>& in_normals,
				std::vector<unsigned int> & out_indices,
				std::vector<VertexFormat> & out_vertices,
				std::vector<glm::vec3>& out_normals);

		private:
			static bool getSimilarVertexIndex(PackedVertex& packed, std::map<PackedVertex, unsigned int> & VertexToOutIndex, unsigned int & result);


		};

	}

}