#pragma once

#include "ILight.h"
#include <glm/glm.hpp>

namespace Engine {

	namespace Rendering {
	
		namespace Lights {
		
			class Light : public ILight {

			public:
				Light();
				Light(const Light& inLight);
				~Light();

				virtual void setPosition(glm::vec3 in_position) override;
				virtual void setDirection(glm::vec3 in_direction) override;
				virtual void setAmbiant(glm::vec3 in_ambiant) override;
				virtual void setDiffuse(glm::vec3 in_diffuse) override;
				virtual void setSpecular(glm::vec3 in_specular) override;
				virtual void setColor(glm::vec3 in_color) override;
				virtual void setLightIntensity(float in_intensity) override;
				virtual void setType(LightType inType) override;
				virtual void setFade(float in_constant, float in_linear, float in_quadratic) override;
				virtual void setCone(float in_innerCutoff, float in_outerCutoff) override;

				glm::vec3 getDirection();

				virtual void update() override;

				virtual LightType getType() override;				
				virtual glm::vec3 getLightAmbiant() override;
				virtual glm::vec3 getLightDiffuse() override;
				virtual glm::vec3 getLightSpecular() override;
				virtual float getLightIntensity() override;
				virtual glm::vec3 getLightPosition() override;
				virtual void getLightProperties(glm::vec3 &out_position, 
					glm::vec3 &out_ambiant, 
					glm::vec3 &out_diffuse,
					glm::vec3 &out_specular,
					float &out_intensity) override;
				virtual void getLightProperties(glm::vec3 &out_position, glm::vec3 &out_ambiant, glm::vec3 &out_diffuse, glm::vec3 &out_specular,
					float &out_constant, float &out_linear, float& out_quadratic, float &out_intensity) override;
				virtual void getLightProperties(glm::vec3 &out_position, glm::vec3 &out_ambiant, glm::vec3 &out_diffuse, glm::vec3 &out_specular, glm::vec3 &out_direction,
					float &out_constant, float &out_linear, float &out_quadratic, float &out_cutoff, float& out_outercutoff, float &out_intensity) override;

			private:
				// ALL
				float intensity;
				glm::vec3 globalColor;
				glm::vec3 position;
				glm::vec3 ambiant;
				glm::vec3 diffuse;
				glm::vec3 specular;
				LightType type;

				// DIRECTIONAL & SPOT
				glm::vec3 direction;

				// POINT
				float constant;
				float linear;
				float quadratic;

				// SPOT
				float innerCutoff;
				float outerCutoff;

			};

		}

	}

}