#pragma once

enum LightType {

	L_DIRECTIONAL = 0,
	L_OMNI = 1,
	L_CONICAL = 2,
	L_CASTER = 3

};