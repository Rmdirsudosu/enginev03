#pragma once

#include <glm/glm.hpp>

namespace Engine {

	namespace Rendering {
	
		namespace Lights {

			enum LightType {
			
				G_OMNI = 0,
				G_DIRECTIONAL = 1,
				G_POINT = 2,
				G_SPOT = 3

			};
		
			class ILight {

			public:
				virtual ~ILight();

				virtual void setPosition(glm::vec3 in_position) = 0;
				virtual void setDirection(glm::vec3 in_direction) = 0;
				virtual void setAmbiant(glm::vec3 in_ambiant) = 0;
				virtual void setDiffuse(glm::vec3 in_diffuse) = 0;
				virtual void setSpecular(glm::vec3 in_specular) = 0;
				virtual void setColor(glm::vec3 in_color) = 0;
				virtual void setLightIntensity(float in_intensity) = 0;
				virtual void setType(LightType inType) = 0;
				virtual void setFade(float in_constant, float in_linear, float in_quadratic) = 0;
				virtual void setCone(float in_innerCutoff, float in_outerCutoff) = 0;

				virtual void update() = 0;

				virtual LightType getType() = 0;				
				virtual glm::vec3 getLightAmbiant() = 0;
				virtual glm::vec3 getLightDiffuse() = 0;
				virtual glm::vec3 getLightSpecular() = 0;
				virtual float getLightIntensity() = 0;
				virtual glm::vec3 getLightPosition() = 0;
				virtual void getLightProperties(glm::vec3 &out_position, glm::vec3 &out_ambiant, glm::vec3 &out_diffuse, glm::vec3 &out_specular, float &out_intensity) = 0;
				virtual void getLightProperties(glm::vec3 &out_position, glm::vec3 &out_ambiant, glm::vec3 &out_diffuse, glm::vec3 &out_specular,
					float &constant, float &linear, float &quadratic, float &out_intensity) = 0;
				virtual void getLightProperties(glm::vec3 &out_position, glm::vec3 &out_ambiant, glm::vec3 &out_diffuse, glm::vec3 &out_specular, glm::vec3 &out_direction,
					float &constant, float &linear, float &quadratic, float &out_innercutoff, float &out_outercutoff, float &out_intensity) = 0;

			};

			inline ILight::~ILight() {
			
			}

		}

	}

}