#include "Light.h"
#include <glm/gtc/matrix_transform.hpp>

namespace Engine {

	namespace Rendering {

		namespace Lights {

			Light::Light()
			{
				position = glm::vec3(0.0f, 0.0f, 0.0f);
				ambiant = glm::vec3(0.2f, 0.2f, 0.2f);
				diffuse = glm::vec3(0.5f, 0.5f, 0.5f);
				specular = glm::vec3(1.0f, 1.0f, 1.0f);
				type = G_OMNI;
			}

			Light::Light(const Light & inLight)
			{
			}

			Light::~Light()
			{
			}

			void Light::setPosition(glm::vec3 in_position)
			{
				position = in_position;
			}

			void Light::setAmbiant(glm::vec3 in_ambiant)
			{
				ambiant = in_ambiant;
			}

			void Light::setDiffuse(glm::vec3 in_diffuse)
			{
				diffuse = in_diffuse;
			}

			void Light::setSpecular(glm::vec3 in_specular)
			{
				specular = in_specular;
			}

			void Light::setColor(glm::vec3 in_color)
			{
				globalColor = in_color;
			}

			void Light::setLightIntensity(float in_intensity)
			{
				intensity = in_intensity;
			}

			void Light::setType(LightType inType)
			{
				type = inType;
			}

			void Light::setFade(float in_constant, float in_linear, float in_quadratic)
			{
				constant = in_constant;
				linear = in_linear;
				quadratic = in_quadratic;
			}

			void Light::setCone(float in_innerCutoff, float in_outerCutoff)
			{
				innerCutoff = in_innerCutoff;
				outerCutoff = in_outerCutoff;
			}

			void Light::setDirection(glm::vec3 in_direction)
			{
				direction = in_direction;
			}

			glm::vec3 Light::getDirection()
			{
				return direction;
			}

			void Light::update()
			{
				/*globalColor.x = sin(glfwGetTime() * 2.0f);
				globalColor.y = sin(glfwGetTime() * 0.7f);
				globalColor.z = sin(glfwGetTime() * 1.3f);

				diffuse = globalColor * glm::vec3(0.5f);
				ambiant = diffuse * glm::vec3(0.2f);*/
			}

			LightType Light::getType()
			{
				return LightType();
			}

			glm::vec3 Light::getLightAmbiant()
			{
				return ambiant;
			}

			glm::vec3 Light::getLightDiffuse()
			{
				return diffuse;
			}

			glm::vec3 Light::getLightSpecular()
			{
				return specular;
			}

			float Light::getLightIntensity()
			{
				return intensity;
			}

			glm::vec3 Light::getLightPosition()
			{
				return position;
			}

			void Light::getLightProperties(glm::vec3 & out_position, glm::vec3 & out_ambiant, glm::vec3 & out_diffuse, glm::vec3 & out_specular, float & out_intensity)
			{
				if (type == G_DIRECTIONAL)
					out_position = direction;
				else
					out_position = position;
				out_ambiant = ambiant;
				out_diffuse = diffuse;
				out_specular = specular;
				out_intensity = intensity;
			}

			void Light::getLightProperties(glm::vec3 & out_position, glm::vec3 & out_ambiant, glm::vec3 & out_diffuse, glm::vec3 & out_specular, float &out_constant, float &out_linear, float &out_quadratic, float & out_intensity)
			{
				out_position = position;
				out_ambiant = ambiant;
				out_diffuse = diffuse;
				out_specular = specular;
				out_intensity = intensity;
				out_constant = constant;
				out_linear = linear;
				out_quadratic = quadratic;
			}

			void Light::getLightProperties(glm::vec3 & out_position, glm::vec3 & out_ambiant, glm::vec3 & out_diffuse, glm::vec3 & out_specular, glm::vec3 &out_direction, 
				float &out_constant, float &out_linear, float &out_quadratic, float& out_innercutoff, float &out_outercutoff, float & out_intensity)
			{
				out_position = position;
				out_direction = direction;
				out_ambiant = ambiant;
				out_diffuse = diffuse;
				out_specular = specular;
				out_direction = direction;
				out_intensity = intensity;
				out_innercutoff = innerCutoff;
				out_outercutoff = outerCutoff;
				out_constant = constant;
				out_linear = linear;
				out_quadratic = quadratic;
			}

		}

	}

}